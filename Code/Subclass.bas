Attribute VB_Name = "Z_Subclass"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  WinAPI\Shared\Core\Constants\Window.bas
'  WinAPI\Shared\Core\Memory.bas
'  WinAPI\Shared\Core\Process.bas
'  WinAPI\Shared\Core\Window.bas
'  WinAPI\Shared\Core\Window.Messaging.bas
'
' Notes:
'
'  Requires subclassed object to implement:
'  - Public Property Get Response() As Long
'  - Public Function WindowProc(ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lwParam As Long, ByVal lParam As Long) As Long
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Private Const CHAR_035 As String = "#"
Private Const CHAR_067 As String = "C"

'DECLARATIONS

Private Declare Function GetPropA Lib "user32" (ByVal lhWnd As Long, ByVal sString As String) As Long
Private Declare Function RemovePropA Lib "user32" (ByVal lhWnd As Long, ByVal lpString As String) As Long
Private Declare Function SetPropA Lib "user32" (ByVal lhWnd As Long, ByVal sString As String, ByVal lData As Long) As Long

'METHODS

Public Sub Subclass_AttachMessage(ByVal lObjPtr As Long, ByVal lhWnd As Long, ByVal lMsg As Long)

    Dim l_Iterator As Long
    Dim l_ProcPtr As Long

    If Window_GetProcessId(lhWnd) = Process_GetCurrentProcessId Then

        For l_Iterator = 1& To GetMessageClassCount(lhWnd, lMsg) 'check if this class is already attached for this message

            If GetMessageClass(lhWnd, lMsg, l_Iterator) = lObjPtr Then Exit Sub

        Next l_Iterator

        If Not SetMessageClassCount(lhWnd, lMsg, GetMessageClassCount(lhWnd, lMsg) + 1&) Then Exit Sub 'associate this class with this message for this window

        If Not SetMessageClass(lhWnd, lMsg, GetMessageClassCount(lhWnd, lMsg), lObjPtr) Then 'associate the class pointer

            SetMessageClassCount lhWnd, lMsg, GetMessageClassCount(lhWnd, lMsg) - 1&

            Exit Sub

        End If

        If GetMessageCount(lhWnd) = 0& Then

            l_ProcPtr = Window_Change(lhWnd, GWL_WNDPROC, AddressOf WindowProc) 'install window procedure

            If l_ProcPtr = 0& Then 'can't subclass

                RemoveMessageClass lhWnd, lMsg

                Exit Sub

            End If

            If Not SetOldWindowProc(lhWnd, l_ProcPtr) Then 'associate old procedure with handle

                Window_Change lhWnd, GWL_WNDPROC, l_ProcPtr 'put the old window procedure back again
                RemoveMessageClass lhWnd, lMsg

                Exit Sub

            End If

        End If

        If Not SetMessageCount(lhWnd, GetMessageCount(lhWnd) + 1&) Then 'count this message

            RemoveMessageClass lhWnd, lMsg

            If GetMessageCount(lhWnd) = 0& Then 'if no messages on this window then remove the subclass

                l_ProcPtr = GetOldWindowProc(lhWnd) 'put old window proc back again

                If l_ProcPtr Then

                    Window_Change lhWnd, GWL_WNDPROC, l_ProcPtr
                    SetOldWindowProc lhWnd, 0&

                End If

            End If

        End If

    End If

End Sub

Public Function Subclass_CallOldWindowProc(ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lwParam As Long, ByVal lParam As Long) As Long

    Dim l_ProcPtr As Long

    l_ProcPtr = GetOldWindowProc(lhWnd)

    If l_ProcPtr Then Subclass_CallOldWindowProc = Window_CallProc(l_ProcPtr, lhWnd, lMsg, lwParam, lParam)

End Function

Public Sub Subclass_DetachMessage(ByVal lObjPtr As Long, ByVal lhWnd As Long, ByVal lMsg As Long)

    Dim l_Count As Long
    Dim l_Iterator As Long
    Dim l_MsgClsCount As Long
    Dim l_ProcPtr As Long

    If Window_GetProcessId(lhWnd) = Process_GetCurrentProcessId Then

        l_MsgClsCount = GetMessageClassCount(lhWnd, lMsg) 'check if this message is attached for this class

        If l_MsgClsCount > 0& Then

            l_Count = 0&

            For l_Iterator = 1& To l_MsgClsCount

                If GetMessageClass(lhWnd, lMsg, l_Iterator) = lObjPtr Then

                    l_Count = l_Iterator

                    Exit For

                End If

            Next l_Iterator

            If l_Count Then 'remove this message class

                For l_Iterator = l_Count To l_MsgClsCount - 1& 'anything above this index has to be shifted up
                    SetMessageClass lhWnd, lMsg, l_Iterator, GetMessageClass(lhWnd, lMsg, l_Iterator + 1&)
                Next l_Iterator

                RemoveMessageClass lhWnd, lMsg

                If GetMessageCount(lhWnd) = 1& Then

                    l_ProcPtr = GetOldWindowProc(lhWnd)

                    If l_ProcPtr Then Window_Change lhWnd, GWL_WNDPROC, l_ProcPtr 'assign old window procedure

                    SetOldWindowProc lhWnd, 0&

                End If

                SetMessageCount lhWnd, GetMessageCount(lhWnd) - 1&

            End If

        End If

    End If

End Sub

Private Function GetMessageClass(ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lIndex As Long) As Long

    GetMessageClass = GetPropA(lhWnd, lhWnd & CHAR_035 & lMsg & CHAR_035 & lIndex)

End Function

Private Function GetMessageClassCount(ByVal lhWnd As Long, ByVal lMsg As Long) As Long

    GetMessageClassCount = GetPropA(lhWnd, lhWnd & CHAR_035 & lMsg & CHAR_067)

End Function

Private Function GetMessageCount(ByVal lhWnd As Long) As Long

    GetMessageCount = GetPropA(lhWnd, CHAR_067 & lhWnd)

End Function

Private Function GetOldWindowProc(ByVal lhWnd As Long) As Long

    GetOldWindowProc = GetPropA(lhWnd, lhWnd)

End Function

Private Sub RemoveMessageClass(ByVal lhWnd As Long, ByVal lMsg As Long)

    Dim l_MsgClsCount As Long

    l_MsgClsCount = GetMessageClassCount(lhWnd, lMsg)

    SetMessageClass lhWnd, lMsg, l_MsgClsCount, 0&
    SetMessageClassCount lhWnd, lMsg, l_MsgClsCount - 1&

End Sub

Private Function SetMessageClass(ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lIndex As Long, ByVal lClassPtr As Long) As Boolean

    Dim l_Property As String

    l_Property = lhWnd & CHAR_035 & lMsg & CHAR_035 & lIndex

    If lClassPtr = 0& Then SetMessageClass = RemovePropA(lhWnd, l_Property) Else SetMessageClass = SetPropA(lhWnd, l_Property, lClassPtr)

End Function

Private Function SetMessageClassCount(ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lCount As Long) As Boolean

    Dim l_Property As String

    l_Property = lhWnd & CHAR_035 & lMsg & CHAR_067

    RemovePropA lhWnd, l_Property

    If lCount Then SetMessageClassCount = SetPropA(lhWnd, l_Property, lCount) Else SetMessageClassCount = True

End Function

Private Function SetMessageCount(ByVal lhWnd As Long, ByVal lCount As Long) As Boolean

    Dim l_Property As String

    l_Property = CHAR_067 & lhWnd

    RemovePropA lhWnd, l_Property

    If lCount Then SetMessageCount = SetPropA(lhWnd, l_Property, lCount) Else SetMessageCount = True

End Function

Private Function SetOldWindowProc(ByVal lhWnd As Long, ByVal lPtr As Long) As Boolean

    If lPtr = 0& Then SetOldWindowProc = RemovePropA(lhWnd, lhWnd) Else SetOldWindowProc = SetPropA(lhWnd, lhWnd, lPtr)

End Function

Private Function WindowProc(ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lwParam As Long, ByVal lParam As Long) As Long

    Dim l_Callback As Object
    Dim l_CallbackT As Object
    Dim l_IsCalled As Boolean
    Dim l_MsgCls As Long
    Dim l_MsgClsCount As Long
    Dim l_ProcPtr As Long

    l_ProcPtr = GetOldWindowProc(lhWnd) 'get the old procedure from the window

    If l_ProcPtr Then

        l_MsgClsCount = GetMessageClassCount(lhWnd, lMsg) 'get the number of instances for this lMsg/lhWnd
        l_IsCalled = False

        If l_MsgClsCount > 0& Then

            Do While l_MsgClsCount >= 1&
                l_MsgCls = GetMessageClass(lhWnd, lMsg, l_MsgClsCount)

                If l_MsgCls Then

                    Memory_Copy l_CallbackT, l_MsgCls, 4& 'turn pointer into a reference
                    Set l_Callback = l_CallbackT
                    Memory_Copy l_CallbackT, 0&, 4&

                    If l_MsgClsCount = 1& And Not l_IsCalled Then

                        If l_Callback.Response = 2& Then 'MSG_PREPROCESS (only checked first time around)

                            WindowProc = Window_CallProc(l_ProcPtr, lhWnd, lMsg, lwParam, lParam)
                            l_IsCalled = True

                        End If

                    End If

                    WindowProc = l_Callback.WindowProc(lhWnd, lMsg, lwParam, lParam) 'this message is always passed to all control instances regardless of whether any single one of them requests to consume it

                End If

                l_MsgClsCount = l_MsgClsCount - 1&
            Loop

            If Not l_Callback Is Nothing And Not l_IsCalled Then

                If l_Callback.Response = 1& Then 'MSG_POSTPROCESS (only check this the last time around)

                    WindowProc = Window_CallProc(l_ProcPtr, lhWnd, lMsg, lwParam, lParam)
                    l_IsCalled = True

                End If

            End If

        Else

            If lMsg = WM_DESTROY Then

                If GetMessageCount(lhWnd) > 0& Then

                    l_MsgCls = GetOldWindowProc(lhWnd)

                    If l_MsgCls Then Window_Change lhWnd, GWL_WNDPROC, l_MsgCls 'unsubclass by reassigning old window procedure

                    SetOldWindowProc lhWnd, 0& 'remove the old window procedure
                    SetMessageCount lhWnd, 0&

                End If

            End If

            WindowProc = Window_CallProc(l_ProcPtr, lhWnd, lMsg, lwParam, lParam)

        End If

    End If

End Function
