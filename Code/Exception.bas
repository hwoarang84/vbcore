Attribute VB_Name = "Z_Exception"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  WinAPI\Shared\Exception.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Private Const CHAR_32 As String = " "
Private Const CHAR_37 As String = "%"
Private Const CHAR_91 As String = "["
Private Const CHAR_93 As String = "]"
Private Const CHAR_94 As String = "^"
Private Const CHAR_123 As String = "{"
Private Const CHAR_125 As String = "}"
Private Const ITERATOR_OFFSET As Long = 1&
Private Const MESSAGE_PATTERN As String = "*" & CHAR_91 & CHAR_91 & "0-9" & CHAR_93 & CHAR_93

'METHODS

Public Sub Exception_Throw(ByVal lError As Long, ByRef sSource As String, ByRef sMessage As String, ParamArray uArguments() As Variant)

    Dim l_DllErrNumber As Long
    Dim l_ErrMessage As String
    Dim l_ErrNumber As Long
    Dim l_Temp As String

    l_DllErrNumber = Err.LastDllError

    If l_DllErrNumber Then

        If Exception_GetSystemMessageById(l_DllErrNumber, l_ErrMessage) Then l_ErrMessage = ConcatMessages(FormatMessage(l_ErrMessage, uArguments, UBound(uArguments), CHAR_37, vbNullString, ITERATOR_OFFSET), l_DllErrNumber)

    End If

    l_ErrNumber = Err.Number

    If l_ErrNumber Then l_ErrMessage = ConcatMessages(Err.Description, l_ErrNumber, l_ErrMessage) Else l_ErrNumber = l_DllErrNumber

    If lError Then l_ErrNumber = lError

    If l_ErrNumber Then

        l_Temp = ConcatMessages(FormatMessage(sMessage, uArguments, UBound(uArguments), CHAR_123, CHAR_125), l_ErrNumber)

        If Left$(l_ErrMessage, Len(l_Temp)) <> l_Temp Then l_ErrMessage = ConcatMessages(l_Temp, 0&, l_ErrMessage)

        If Len(l_ErrMessage) Then Err.Raise l_ErrNumber, sSource, l_ErrMessage Else Err.Raise l_ErrNumber, sSource

    End If

End Sub

Private Function ConcatMessages(ByRef sMessage1 As String, ByVal lErrorNumber As Long, Optional ByRef sMessage2 As String) As String

    Dim l_Message1 As String

    If Len(sMessage1) Then

        If lErrorNumber = 0& Or sMessage1 Like MESSAGE_PATTERN Then l_Message1 = sMessage1 Else l_Message1 = sMessage1 & CHAR_32 & CHAR_91 & lErrorNumber & CHAR_93

        If Len(sMessage2) Then ConcatMessages = l_Message1 & vbCrLf & CHAR_94 & CHAR_32 & sMessage2 Else ConcatMessages = l_Message1

    Else

        ConcatMessages = sMessage2

    End If

End Function

Private Function FormatMessage(ByRef sMessage As String, ByVal uArguments As Variant, ByVal lUBound As Long, ByRef sChar1 As String, ByRef sChar2 As String, Optional ByVal lOffset As Long) As String

    Dim l_Iterator As Long
    Dim l_Result As String

    l_Result = sMessage

    If lUBound >= 0& Then

        For l_Iterator = 0& To lUBound
            l_Result = Replace(l_Result, sChar1 & l_Iterator + lOffset & sChar2, uArguments(l_Iterator), , , vbBinaryCompare)
        Next l_Iterator

    End If

    FormatMessage = l_Result

End Function
