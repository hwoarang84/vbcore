Attribute VB_Name = "Z_Logger"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  WinAPI\Shared\Core\Types\DateTime.bas
'  WinAPI\Shared\Core\DateTime.bas
'  WinAPI\Shared\Core\Memory.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_LOGGER_TYPE
    LOG_TYPE_ERROR& = 3&
    LOG_TYPE_INFO& = 1&
    LOG_TYPE_RECORD& = 0&
    LOG_TYPE_WARNING& = 2&
End Enum

'TYPES

Public Type TLOGGERITEM
    sTimeStamp As String
    sCallStack As String
    sMessage As String
    lType As ENUM_LOGGER_TYPE
End Type

'CONSTANTS

Private Const CONST_DEFAULT As Long = -1&
Private Const CONST_MILLIES_FORMAT As String = "000"
Private Const CONST_STACK_COL_COUNT As Long = 4&
Private Const CONST_STACK_COL_SIZE As Long = 4&
Private Const CONST_STACK_ROWS_COUNT_MAX As Long = 4096&
Private Const CONST_STACK_ROWS_COUNT_MIN As Long = 64&
Private Const CONST_STACK_SEPARATOR As String = "."

'VARIABLES

Private m_CallStack As String
Private m_CallStackLen As Long
Private m_CallStackPointer As Long
Private m_CallStackSize As Long
Private m_Stack() As TLOGGERITEM
Private m_StackCount As Long
Private m_StackSize As Long
Private m_StackPointer As Long

'METHODS

Public Sub Log_Add(ByRef sCallStackRoot As String, ByVal lType As ENUM_LOGGER_TYPE, ByRef sRoutine As String, ByRef sMessage As String, ParamArray uArguments() As Variant)

    Dim l_SystemTime As TSYSTEMTIME

    EnsureStackHasEnoughCapacity

    DateTime_GetLocal l_SystemTime

    m_Stack(m_StackCount).sTimeStamp = (DateSerial(l_SystemTime.iYear, l_SystemTime.iMonth, l_SystemTime.iDay) & ChrW$(32&) & _
       TimeSerial(l_SystemTime.iHour, l_SystemTime.iMinute, l_SystemTime.iSecond)) & ChrW$(46&) & Format$(l_SystemTime.iMilliseconds, CONST_MILLIES_FORMAT)

    If Len(sCallStackRoot) Then TrimStack sCallStackRoot

    TrimStack sRoutine

    If m_CallStackLen Then m_Stack(m_StackCount).sCallStack = Left$(m_CallStack, m_CallStackLen + CONST_DEFAULT)

    If Len(sMessage) Then m_Stack(m_StackCount).sMessage = FormatPlaceholders(sMessage, uArguments)

    m_Stack(m_StackCount).lType = lType

    m_StackCount = m_StackCount + 1&

End Sub

Public Sub Log_Clear()

    m_CallStack = vbNullString
    m_CallStackLen = 0&
    m_CallStackPointer = 0&
    m_StackCount = 0&
    m_StackSize = 0&
    m_StackPointer = 0&

    Erase m_Stack

End Sub

Public Function Log_Read(ByVal lIndex As Long) As TLOGGERITEM

    Dim l_RowSize As Long

    If m_StackSize And lIndex >= 0& And lIndex <= m_StackCount Then

        l_RowSize = CONST_STACK_COL_COUNT * CONST_STACK_COL_SIZE

        Memory_Copy ByVal Log_Read, ByVal m_StackPointer + (lIndex * l_RowSize), l_RowSize

    End If

End Function

Public Function Log_ReadBulk(Optional ByVal lType As ENUM_LOGGER_TYPE) As TLOGGERITEM()

    On Error GoTo ErrorHandler

    Dim l_Count As Long
    Dim l_Iterator As Long
    Dim l_LoggerItems() As TLOGGERITEM

    If m_StackCount Then

        ReDim l_LoggerItems(m_StackCount + CONST_DEFAULT)

        If lType = LOG_TYPE_RECORD Then

            l_Count = m_StackCount * (CONST_STACK_COL_COUNT * CONST_STACK_COL_SIZE)

            Memory_Copy ByVal VarPtr(l_LoggerItems(0)), ByVal m_StackPointer, l_Count

        Else

            For l_Iterator = 0& To m_StackCount + CONST_DEFAULT

                If m_Stack(l_Iterator).lType = lType Then

                    l_LoggerItems(l_Count) = m_Stack(l_Iterator)
                    l_Count = l_Count + 1&

                End If

            Next l_Iterator

            If l_Count Then ReDim Preserve l_LoggerItems(l_Count + CONST_DEFAULT)

        End If

        If l_Count Then Log_ReadBulk = l_LoggerItems

        Erase l_LoggerItems

    End If

ErrorHandler:

End Function

Public Sub Log_Remove(ByVal lIndex As Long)

    Dim l_RowSize As Long

    If m_StackSize And lIndex >= 0& And lIndex <= m_StackCount Then

        l_RowSize = CONST_STACK_COL_COUNT * CONST_STACK_COL_SIZE

        Memory_Copy ByVal m_StackPointer + (lIndex * l_RowSize), ByVal m_StackPointer + ((lIndex + 1&) * l_RowSize), (m_StackCount - lIndex) * l_RowSize
        Memory_Clear ByVal m_StackPointer + (m_StackCount * l_RowSize), l_RowSize

        m_StackCount = m_StackCount + CONST_DEFAULT

    End If

End Sub

Private Sub EnsureStackHasEnoughCapacity()

    Dim l_RowSize As Long
    Dim l_ShiftSize As Long

    If m_StackCount - CONST_DEFAULT > m_StackSize Then

        If m_StackSize < CONST_STACK_ROWS_COUNT_MAX Then

            m_StackSize = m_StackSize + CONST_STACK_ROWS_COUNT_MIN

            ReDim Preserve m_Stack(m_StackSize + CONST_DEFAULT)

            m_StackPointer = VarPtr(m_Stack(0&))

        Else

            l_RowSize = CONST_STACK_COL_COUNT * CONST_STACK_COL_SIZE
            l_ShiftSize = (m_StackSize + CONST_DEFAULT) * l_RowSize

            Memory_Copy ByVal m_StackPointer, ByVal m_StackPointer + l_RowSize, l_ShiftSize 'shifs all items to one position higher
            Memory_Clear ByVal m_StackPointer + l_ShiftSize, l_RowSize

            m_StackCount = m_StackSize + CONST_DEFAULT

        End If

    End If

End Sub

Private Function FormatPlaceholders(ByRef sTarget As String, ParamArray uArguments() As Variant) As String

    Dim l_Iterator As Long
    Dim l_Result As String

    l_Result = sTarget

    For l_Iterator = 0& To UBound(uArguments(0))
        l_Result = Replace(l_Result, ChrW$(123&) & l_Iterator & ChrW$(125&), uArguments(0)(l_Iterator))
    Next l_Iterator

    FormatPlaceholders = l_Result

End Function

Private Sub TrimStack(ByRef sTrimTo As String)

    Dim l_Buffer As String
    Dim l_Length As Long
    Dim l_Position As Long
    Dim l_Resize As Boolean

    l_Length = Len(sTrimTo)

    If l_Length Then

        If m_CallStackLen Then l_Position = InStrRev(m_CallStack, sTrimTo & CONST_STACK_SEPARATOR, m_CallStackLen + 1&, vbBinaryCompare)

        If l_Position Then

            m_CallStackLen = l_Position + l_Length

        Else

            m_CallStackLen = m_CallStackLen + l_Length + 1&

            If m_CallStackLen > (m_CallStackLen \ 2&) Then m_CallStackSize = m_CallStackSize + (m_CallStackLen * 2&) Else l_Resize = m_CallStackPointer

            If Not l_Resize Then

                l_Position = m_CallStackLen - l_Length + CONST_DEFAULT

                If l_Position Then l_Buffer = Left$(m_CallStack, l_Position)

                m_CallStack = Space$(m_CallStackSize)
                m_CallStackPointer = StrPtr(m_CallStack)

                If l_Position Then Memory_Copy ByVal m_CallStackPointer, ByVal StrPtr(l_Buffer), l_Position * 2&

            End If

            Memory_Copy ByVal m_CallStackPointer + ((m_CallStackLen - l_Length + CONST_DEFAULT) * 2&), ByVal StrPtr(sTrimTo & CONST_STACK_SEPARATOR), (l_Length + 1&) * 2&

        End If

    Else

        m_CallStackLen = 0&

    End If

End Sub
