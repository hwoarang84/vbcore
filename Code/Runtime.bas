Attribute VB_Name = "Z_Runtime"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  WinAPI\Shared\Core\Process.bas
'  WinAPI\Shared\Core\Window.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Private Const IDE_WINDOW_NAME As String = "IDEOwner"

'VARIABLES

Private m_IsIDEChecked As Boolean
Private m_IsIDE As Boolean

Public Function Runtime_IsIDE() As Boolean

    If Not m_IsIDEChecked Then

        m_IsIDE = Process_GetCurrentProcessId = Window_GetProcessId(Window_FindByClass(IDE_WINDOW_NAME))
        m_IsIDEChecked = True

    End If

    Runtime_IsIDE = m_IsIDE

End Function
