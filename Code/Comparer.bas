Attribute VB_Name = "Z_Comparer"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  WinAPI\Shared\Core\String.bas
'  WinAPI\Shared\Core\String.Compare.bas
'  WinAPI\Shared\SafeArray.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_COMPARISON
    COMPARISON_EQUAL& = 0&
    COMPARISON_GREATER& = 1&
    COMPARISON_LESS& = -1&
End Enum

'METHODS

Public Function Compare_Any(ByRef Value1 As Variant, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    If IsObject(Value1) Then

        Compare_Any = CompareObjectInternal(Value1, Value2)

    ElseIf IsArray(Value1) Then

        Compare_Any = CompareArray(Value1, Value2)

    Else

        Select Case VarType(Value1)

            Case vbBoolean, vbByte, vbDecimal, vbError, vbInteger To vbDate: Compare_Any = CompareNumeric(Value1, Value2, CompareMethod)

            Case vbString: Compare_Any = CompareStringInternal(Value1, Value2, CompareMethod)

            Case vbEmpty: Compare_Any = CompareEmpty(Value2)

            Case vbNull: Compare_Any = CompareNull(Value2)

        End Select

    End If

End Function

Public Function Compare_Boolean(ByVal Value1 As Boolean, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_Boolean = CompareNumeric(Value1, Value2, CompareMethod)

End Function

Public Function Compare_Byte(ByVal Value1 As Byte, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_Byte = CompareNumeric(Value1, Value2, CompareMethod)

End Function

Public Function Compare_Currency(ByVal Value1 As Currency, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_Currency = CompareNumeric(Value1, Value2, CompareMethod)

End Function

Public Function Compare_Date(ByVal Value1 As Date, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_Date = CompareNumeric(Value1, Value2, CompareMethod)

End Function

Public Function Compare_Double(ByVal Value1 As Double, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_Double = CompareNumeric(Value1, Value2, CompareMethod)

End Function

Public Function Compare_Integer(ByVal Value1 As Integer, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_Integer = CompareNumeric(Value1, Value2, CompareMethod)

End Function

Public Function Compare_Long(ByVal Value1 As Long, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_Long = CompareNumeric(Value1, Value2, CompareMethod)

End Function

Public Function Compare_Object(ByRef Value1 As Object, ByRef Value2 As Variant) As ENUM_COMPARISON

    Compare_Object = CompareObjectInternal(Value1, Value2)

End Function

Public Function Compare_Single(ByVal Value1 As Single, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_Single = CompareNumeric(Value1, Value2, CompareMethod)

End Function

Public Function Compare_String(ByRef Value1 As String, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_String = CompareStringInternal(Value1, Value2, CompareMethod)

End Function

Public Function Compare_StringByPointer(ByVal Value1 As Long, ByVal Length As Long, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    Compare_StringByPointer = CompareStringInternal(Value1, Value2, CompareMethod, Length)

End Function

Private Function CompareArray(ByRef Value1 As Variant, ByRef Value2 As Variant) As ENUM_COMPARISON

    Dim l_Array1 As TSAFEARRAY
    Dim l_Array2 As TSAFEARRAY
    Dim l_Count1 As Long
    Dim l_Count2 As Long
    Dim l_Iterator As Long

    If IsObject(Value2) Then

        CompareArray = InvertComparison(GetComparisonOfObjectAndArray(Value2, Value1))

    ElseIf IsArray(Value2) Then

        If SafeArray_FillInfo(l_Array1, VarPtr(Value1)) = S_OK Then

            If SafeArray_FillInfo(l_Array2, VarPtr(Value2)) = S_OK Then

                For l_Iterator = 1& To l_Array1.iDims
                    l_Count1 = l_Count1 + SafeArray_GetElementsCount(l_Array1, l_Iterator) * l_Array1.lElementSize
                Next l_Iterator

                For l_Iterator = 1& To l_Array2.iDims
                    l_Count2 = l_Count2 + SafeArray_GetElementsCount(l_Array2, l_Iterator) * l_Array2.lElementSize
                Next l_Iterator

                If l_Count1 > l_Count2 Then

                    CompareArray = COMPARISON_GREATER

                ElseIf l_Count1 < l_Count2 Then

                    CompareArray = COMPARISON_LESS

                End If

            Else

                CompareArray = COMPARISON_GREATER

            End If

        Else

            CompareArray = l_Array2.lPointer > 0&

        End If

    Else

        Select Case VarType(Value2)

            Case vbEmpty, vbNull: CompareArray = GetComparisonOfAnyAndEmptyOrNull

            Case Else: CompareArray = GetComparisonOfArrayAndValueType(Value1)

        End Select

    End If

End Function

Private Function CompareEmpty(ByRef Value As Variant) As ENUM_COMPARISON

    If IsObject(Value) Then

        CompareEmpty = InvertComparison(GetComparisonOfAnyAndEmptyOrNull)

    Else

        If IsNull(Value) Then CompareEmpty = COMPARISON_GREATER Else CompareEmpty = Not IsEmpty(Value)

    End If

End Function

Private Function CompareNull(ByRef Value As Variant) As ENUM_COMPARISON

    If IsObject(Value) Then CompareNull = InvertComparison(GetComparisonOfAnyAndEmptyOrNull) Else CompareNull = Not IsNull(Value)

End Function

Private Function CompareNumeric(ByRef Value1 As Variant, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As ENUM_COMPARISON

    If IsObject(Value2) Then

        CompareNumeric = InvertComparison(GetComparisonOfObjectAndValueType(Value2))

    ElseIf IsArray(Value2) Then

        CompareNumeric = InvertComparison(GetComparisonOfArrayAndValueType(Value2))

    Else

        Select Case VarType(Value2)

            Case vbString

                CompareNumeric = InvertComparison(GetComparisonOfStringAndValueType(Value2, Value1, CompareMethod))

            Case vbEmpty, vbNull

                CompareNumeric = GetComparisonOfAnyAndEmptyOrNull

            Case Else

                If Value1 > Value2 Then

                    CompareNumeric = COMPARISON_GREATER

                ElseIf Value1 < Value2 Then

                    CompareNumeric = COMPARISON_LESS

                End If

        End Select

    End If

End Function

Private Function CompareObjectInternal(ByRef Value1 As Variant, ByRef Value2 As Variant) As ENUM_COMPARISON

    If IsObject(Value2) Then

        If Not Value1 Is Value2 Then

            If Not Value1 Is Nothing Then

                If Not Value2 Is Nothing Then CompareObjectInternal = COMPARISON_LESS Else CompareObjectInternal = COMPARISON_GREATER

            Else

                CompareObjectInternal = COMPARISON_LESS

            End If

        End If

    ElseIf IsArray(Value2) Then

        CompareObjectInternal = GetComparisonOfObjectAndArray(Value1, Value2)

    Else

        Select Case VarType(Value2)

            Case vbEmpty, vbNull: CompareObjectInternal = GetComparisonOfAnyAndEmptyOrNull

            Case Else: CompareObjectInternal = GetComparisonOfObjectAndValueType(Value1)

        End Select

    End If

End Function

Private Function CompareStringInternal(ByRef Value1 As Variant, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod, Optional ByVal Length As Long) As ENUM_COMPARISON

    If IsObject(Value2) Then

        CompareStringInternal = InvertComparison(GetComparisonOfObjectAndValueType(Value2))

    ElseIf IsArray(Value2) Then

        CompareStringInternal = InvertComparison(GetComparisonOfArrayAndValueType(Value2))

    Else

        Select Case VarType(Value2)

            Case vbEmpty, vbNull: CompareStringInternal = GetComparisonOfAnyAndEmptyOrNull

            Case Else: CompareStringInternal = GetComparisonOfStringAndValueType(Value1, Value2, CompareMethod, Length)

        End Select

    End If

End Function

Private Function GetComparisonOfAnyAndEmptyOrNull() As ENUM_COMPARISON

    GetComparisonOfAnyAndEmptyOrNull = COMPARISON_GREATER

End Function

Private Function GetComparisonOfArrayAndValueType(ByRef ArrayValue As Variant) As ENUM_COMPARISON

    Dim l_Array As TSAFEARRAY

    If SafeArray_FillInfo(l_Array, VarPtr(ArrayValue)) = S_OK Then GetComparisonOfArrayAndValueType = COMPARISON_GREATER Else GetComparisonOfArrayAndValueType = COMPARISON_LESS

End Function

Private Function GetComparisonOfObjectAndArray(ByRef ObjectValue As Variant, ByRef ArrayValue As Variant) As ENUM_COMPARISON

    If ObjectValue Is Nothing Then

        GetComparisonOfObjectAndArray = InvertComparison(GetComparisonOfArrayAndValueType(ArrayValue))

    Else

        GetComparisonOfObjectAndArray = COMPARISON_GREATER

    End If

End Function

Private Function GetComparisonOfObjectAndValueType(ByRef ObjectValue As Variant) As ENUM_COMPARISON

    If ObjectValue Is Nothing Then GetComparisonOfObjectAndValueType = COMPARISON_LESS Else GetComparisonOfObjectAndValueType = COMPARISON_GREATER

End Function

Private Function GetComparisonOfStringAndValueType(ByRef StringValue As Variant, ByRef NumericValue As Variant, ByVal CompareMethod As VbCompareMethod, Optional ByVal StringLength As Long) As ENUM_COMPARISON

    Dim l_IsString As Boolean
    Dim l_LengthNum As Long
    Dim l_LengthStr As Long
    Dim l_Pointer As Long

    l_IsString = VarType(StringValue) = vbString

    If l_IsString Then l_Pointer = StrPtr(StringValue) Else l_Pointer = StringValue

    If StringLength > 0& Then

        l_LengthStr = StringLength

    Else

        If l_IsString Then l_LengthStr = Len(StringValue) Else l_LengthStr = String_GetCharsCount(l_Pointer)

    End If

    l_LengthNum = Len(NumericValue)

    If l_LengthStr > l_LengthNum Then

        GetComparisonOfStringAndValueType = COMPARISON_GREATER

    ElseIf l_LengthStr < l_LengthNum Then

        GetComparisonOfStringAndValueType = COMPARISON_LESS

    ElseIf l_LengthStr > 0& And l_LengthNum > 0& Then

        GetComparisonOfStringAndValueType = String_Compare(CompareMethod, l_Pointer, l_LengthStr, StrPtr(NumericValue), l_LengthNum)

    End If

End Function

Private Function InvertComparison(ByVal Value As ENUM_COMPARISON) As ENUM_COMPARISON

    InvertComparison = (Not Value) + COMPARISON_GREATER

End Function
