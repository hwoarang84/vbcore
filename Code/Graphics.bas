Attribute VB_Name = "Z_Graphics"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  WinAPI\Shared\Gdi.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_GRAPHICS_ALIGN
    GRAPHICS_BOTTOM_CENTER& = 17&
    GRAPHICS_BOTTOM_LEFT& = 16&
    GRAPHICS_BOTTOM_RIGHT& = 18&
    GRAPHICS_CENTER_CENTER& = 14&
    GRAPHICS_CENTER_LEFT& = 13&
    GRAPHICS_CENTER_RIGHT& = 15&
    GRAPHICS_STRETCH& = 0&
    GRAPHICS_TOP_CENTER& = 11&
    GRAPHICS_TOP_LEFT& = 10&
    GRAPHICS_TOP_RIGHT& = 12&
    GRAPHICS_ZOOM_BOTTOM_CENTER& = 8&
    GRAPHICS_ZOOM_BOTTOM_LEFT& = 7&
    GRAPHICS_ZOOM_BOTTOM_RIGHT& = 9&
    GRAPHICS_ZOOM_CENTER_CENTER& = 5&
    GRAPHICS_ZOOM_CENTER_LEFT& = 4&
    GRAPHICS_ZOOM_CENTER_RIGHT& = 6&
    GRAPHICS_ZOOM_TOP_CENTER& = 2&
    GRAPHICS_ZOOM_TOP_LEFT& = 1&
    GRAPHICS_ZOOM_TOP_RIGHT& = 3&
End Enum

'TYPES

Private Type TCALCRECT
    lX As Long
    lY As Long
    lWidth As Long
    lHeight As Long
End Type

'CONSTANTS

Private Const BIT_MASK As Long = 256&

'METHODS

Public Function Draw_Gradient(ByVal lHdc As Long, ByVal lLeft As Long, ByVal lTop As Long, ByRef lWidth As Long, ByRef lHeight As Long, ByVal lStartColor As Long, ByVal lEndColor As Long, Optional ByVal bIsVertical As Boolean) As Boolean

    Dim l_Iterator1 As Long
    Dim l_Iterator2 As Long
    Dim l_Color1(2) As Long
    Dim l_Color2(2) As Long
    Dim l_Color3(2) As Long
    Dim l_Pen As Long
    Dim l_Result As Boolean
    Dim l_Width As Long

    For l_Iterator1 = 0& To 2&
        l_Color1(l_Iterator1) = (lStartColor \ (BIT_MASK ^ l_Iterator1)) Mod 256&
        l_Color2(l_Iterator1) = (lEndColor \ (BIT_MASK ^ l_Iterator1)) Mod 256&
    Next l_Iterator1

    If bIsVertical Then l_Width = lHeight Else l_Width = lWidth

    For l_Iterator1 = 0& To l_Width

        For l_Iterator2 = 0& To 2&

            If l_Color2(l_Iterator2) > l_Color1(l_Iterator2) Then

                l_Color3(l_Iterator2) = l_Color1(l_Iterator2) + (((l_Color2(l_Iterator2) - l_Color1(l_Iterator2)) / l_Width) * l_Iterator1)

            Else

                If l_Color2(l_Iterator2) < l_Color1(l_Iterator2) Then

                    l_Color3(l_Iterator2) = l_Color1(l_Iterator2) - (((l_Color1(l_Iterator2) - l_Color2(l_Iterator2)) / l_Width) * l_Iterator1)

                Else

                    l_Color3(l_Iterator2) = l_Color1(l_Iterator2)

                End If

            End If

        Next l_Iterator2

        l_Pen = Gdi_CreatePen(l_Color3(2) * &H10000 + l_Color3(1) * BIT_MASK + l_Color3(0), 0&, PS_SOLID)

        If l_Pen Then

            Gdi_SelectObject lHdc, l_Pen

            If bIsVertical Then

                Gdi_DrawLine lHdc, lLeft, l_Iterator1 + lTop, lWidth + lLeft, l_Iterator1 + lTop

            Else

                Gdi_DrawLine lHdc, l_Iterator1 + lLeft, lTop, l_Iterator1 + lLeft, lHeight + lTop

            End If

            l_Result = Gdi_DeleteObject(l_Pen)

        Else

            l_Result = False

            Exit For

        End If

    Next l_Iterator1

    Draw_Gradient = l_Result

End Function

Public Function Draw_Picture(ByVal lHdc As Long, ByVal lPictureHandle As Long, Optional ByVal lX As Long, Optional ByVal lY As Long, Optional ByVal lWidth As Long, Optional ByVal lHeight As Long, Optional ByVal lAlignment As ENUM_GRAPHICS_ALIGN, Optional ByVal bTransparency As Byte) As Boolean

    Dim l_Hdc As Long
    Dim l_PictHeight As Long
    Dim l_PictWidth As Long
    Dim l_Rect As TCALCRECT

    If Gdi_GetImageInfo(lPictureHandle, l_PictHeight, l_PictWidth, 0&) Then

        l_Hdc = Gdi_CreateDC(lHdc)

        If l_Hdc Then

            Gdi_SelectObject l_Hdc, lPictureHandle

            If lWidth > 0& And lHeight > 0& Then

                l_Rect = CalculateRect(l_PictWidth, l_PictHeight, lX, lY, lWidth, lHeight, lAlignment)

            Else

                l_Rect.lX = lX
                l_Rect.lY = lY
                l_Rect.lHeight = l_PictHeight
                l_Rect.lWidth = l_PictWidth

            End If

            Draw_Picture = Gdi_DrawImage(l_Hdc, lHdc, l_PictWidth, l_PictHeight, l_Rect.lX, l_Rect.lY, l_Rect.lWidth, l_Rect.lHeight, bTransparency)

            Gdi_DeleteDC l_Hdc

        End If

    End If

End Function

Private Function CalculateRect(ByVal lSrcWidth As Long, ByVal lSrcHeight As Long, ByVal lX As Long, ByVal lY As Long, ByVal lWidth As Long, ByVal lHeight As Long, ByVal lAlign As ENUM_GRAPHICS_ALIGN) As TCALCRECT

    Dim l_Quantity As Double

    With CalculateRect

        If lAlign > GRAPHICS_STRETCH And lAlign < GRAPHICS_TOP_LEFT Then

            l_Quantity = lSrcWidth / lSrcHeight

            .lWidth = lWidth
            .lHeight = lWidth / l_Quantity

            If .lHeight > lHeight Then

                .lHeight = lHeight
                .lWidth = lHeight * l_Quantity

            End If

        ElseIf lAlign = GRAPHICS_STRETCH Then

            .lWidth = lWidth
            .lHeight = lHeight

        Else

            .lWidth = lSrcWidth
            .lHeight = lSrcHeight

        End If

        .lX = lX
        .lY = lY

        Select Case lAlign

            Case GRAPHICS_ZOOM_TOP_CENTER, GRAPHICS_TOP_CENTER

                .lX = .lX + ((lWidth - .lWidth) \ 2&)

            Case GRAPHICS_ZOOM_TOP_RIGHT, GRAPHICS_TOP_RIGHT

                .lX = .lX + (lWidth - .lWidth)

            Case GRAPHICS_ZOOM_CENTER_LEFT, GRAPHICS_CENTER_LEFT

                .lY = .lY + ((lHeight - .lHeight) \ 2&)

            Case GRAPHICS_ZOOM_CENTER_CENTER, GRAPHICS_CENTER_CENTER

                .lX = .lX + ((lWidth - .lWidth) \ 2&)
                .lY = .lY + ((lHeight - .lHeight) \ 2&)

            Case GRAPHICS_ZOOM_CENTER_RIGHT, GRAPHICS_CENTER_RIGHT

                .lX = .lX + (lWidth - .lWidth)
                .lY = .lY + ((lHeight - .lHeight) \ 2&)

            Case GRAPHICS_ZOOM_BOTTOM_LEFT, GRAPHICS_BOTTOM_LEFT

                .lY = .lY + (lHeight - .lHeight)

            Case GRAPHICS_ZOOM_BOTTOM_CENTER, GRAPHICS_BOTTOM_CENTER

                .lX = .lX + ((lWidth - .lWidth) \ 2&)
                .lY = .lY + (lHeight - .lHeight)

            Case GRAPHICS_ZOOM_BOTTOM_RIGHT, GRAPHICS_BOTTOM_RIGHT

                .lX = .lX + (lWidth - .lWidth)
                .lY = .lY + (lHeight - .lHeight)

        End Select

    End With

End Function
