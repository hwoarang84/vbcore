Attribute VB_Name = "Z_Enumerator"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  WinAPI\Shared\Core\Enums\Ole.bas
'  WinAPI\Shared\Core\Memory.bas
'  WinAPI\Shared\Core\Memory.Alloc.bas
'  stdole2.tlb (OLE Automation)
'
' Notes:
'
'  Requires enumerable object to implement:
'  - Public Property Get Count() As Long
'  - Public Property Get Item() As Variant
'
'  Copyright � 2017 Dexter Freivald. All Rights Reserved. DEXWERX.COM
'  See original source at http://www.vbforums.com/showthread.php?854963-VB6-IEnumVARIANT-For-Each-support-without-a-typelib
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'TYPES

Private Type TENUMERATOR
    lVTablePtr As Long
    lReferences As Long
    uEnumerable As Object
    lIndex As Long
    lUpper As Long
End Type

'VARIABLES

Private m_Table(6) As Long

'DECLARATIONS

Private Declare Function VariantCopy Lib "oleaut32" (ByVal lTarget As Long, ByRef uSource As Variant) As Long

'METHODS

Private Function IEnumVARIANT_Clone(ByRef This As TENUMERATOR, ByVal lEnum As Long) As Long

    IEnumVARIANT_Clone = E_NOTIMPL

End Function

Private Function IEnumVARIANT_Next(ByRef This As TENUMERATOR, ByVal lCelt As Long, ByVal lVar As Long, ByVal lFetched As Long) As Long

    Dim l_Count As Long

    With This

        l_Count = .uEnumerable.Count - 1&

        If .lIndex > .lUpper Then

            IEnumVARIANT_Next = 1&

        Else

            If l_Count <> .lUpper Then

                If l_Count < .lUpper Then .lIndex = .lIndex - 1&

                .lUpper = l_Count

            End If

            VariantCopy lVar, .uEnumerable.Item(.lIndex)

            .lIndex = .lIndex + 1&

        End If

    End With

End Function

Private Function IEnumVARIANT_Reset(ByRef This As TENUMERATOR) As Long

    IEnumVARIANT_Reset = E_NOTIMPL

End Function

Private Function IEnumVARIANT_Skip(ByRef This As TENUMERATOR, ByVal lCelt As Long) As Long

    IEnumVARIANT_Skip = E_NOTIMPL

End Function

Private Function IUnknown_AddRef(ByRef This As TENUMERATOR) As Long

    With This

        .lReferences = .lReferences + 1&

        IUnknown_AddRef = .lReferences

    End With

End Function

Private Function IUnknown_QueryInterface(ByRef This As TENUMERATOR, ByVal lRiid As Long, ByVal lObject As Long) As Long

    Memory_Get4 VarPtr(This), ByVal lObject

    This.lReferences = This.lReferences + 1&

End Function

Private Function IUnknown_Release(ByRef This As TENUMERATOR) As Long

    With This

        .lReferences = .lReferences - 1&

        IUnknown_Release = .lReferences

        If .lReferences = 0& Then

            Set .uEnumerable = Nothing

            Memory_ComFree VarPtr(This)

        End If

    End With

End Function

Public Function NewEnumerator(ByRef uEnumerable As Object, ByVal lUpper As Long) As IEnumVARIANT

    Dim l_Enumerator As TENUMERATOR
    Dim l_MemPtr As Long

    If m_Table(0) = 0& Then

        m_Table(0) = Memory_CopyByRef4(AddressOf IUnknown_QueryInterface)
        m_Table(1) = Memory_CopyByRef4(AddressOf IUnknown_AddRef)
        m_Table(2) = Memory_CopyByRef4(AddressOf IUnknown_Release)
        m_Table(3) = Memory_CopyByRef4(AddressOf IEnumVARIANT_Next)
        m_Table(4) = Memory_CopyByRef4(AddressOf IEnumVARIANT_Skip)
        m_Table(5) = Memory_CopyByRef4(AddressOf IEnumVARIANT_Reset)
        m_Table(6) = Memory_CopyByRef4(AddressOf IEnumVARIANT_Clone)

    End If

    With l_Enumerator
        .lVTablePtr = VarPtr(m_Table(0))
        .lUpper = lUpper
        .lReferences = 1&
        Set .uEnumerable = uEnumerable
    End With

    l_MemPtr = Memory_ComAlloc(LenB(l_Enumerator))

    Memory_CopyZero LenB(l_Enumerator), ByVal l_MemPtr, l_Enumerator
    Memory_Get4 l_MemPtr, NewEnumerator

End Function
