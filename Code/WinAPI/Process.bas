Attribute VB_Name = "Z__WinAPI_Process"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Shared\Core\Constants\FileSystem.bas
'  Shared\Core\Enums\Window.bas
'  Shared\String.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_PROCESS_PRIORITY
    ABOVE_NORMAL_PRIORITY_CLASS& = 32768
    BELOW_NORMAL_PRIORITY_CLASS& = 16384&
    HIGH_PRIORITY_CLASS& = 128&
    IDLE_PRIORITY_CLASS& = 64&
    NORMAL_PRIORITY_CLASS& = 32&
    REALTIME_PRIORITY_CLASS& = 256&
End Enum

'TYPES

Private Type TPROCESSINFO
    lProcess As Long
    lThread As Long
    lProcessId As Long
    lThreadId As Long
End Type

Private Type TSTARTUPINFO
    lSize As Long
    sReserved As String
    sDesktop As String
    sTitle As String
    lX As Long
    lY As Long
    lXSize As Long
    lYSize As Long
    lXCountChars As Long
    lYCountChars As Long
    lFillAttribute As Long
    lFlags As Long
    iShowWindow As Integer
    iReserved2 As Integer
    bReserved2 As Byte
    lStdInput As Long
    lStdOutput As Long
    lStdError As Long
End Type

'DECLARATIONS

Public Declare Function Process_GetProcessId Lib "kernel32" Alias "GetProcessId" (ByVal lHandle As Long) As Long

Private Declare Function CreateProcessW Lib "kernel32" (ByVal lApplication As Long, ByVal lCommandLine As Long, ByVal lProcessAttributes As Long, ByVal lThreadAttributes As Long, ByVal lInheritHandles As Long, ByVal lFlags As Long, ByVal lEnvironment As Long, ByVal lCurrentDirectory As Long, ByVal lpProcessInfo As Long, uProcessInfoRet As TPROCESSINFO) As Long
Private Declare Function GetCommandLineW Lib "kernel32" () As Long
Private Declare Function GetCurrentDirectoryW Lib "kernel32" (ByVal lBufferLen As Long, ByVal lBuffer As Long) As Long
Private Declare Function GetModuleFileNameExW Lib "psapi" (ByVal lHandle As Long, ByVal lModule As Long, ByVal lFileName As Long, ByVal lSize As Long) As Long
Private Declare Function GetPriorityClass Lib "kernel32" (ByVal lHandle As Long) As Long
Private Declare Function IsWow64Process Lib "kernel32" (ByVal lHandle As Long, lWow64Process As Long) As Long
Private Declare Function SetCurrentDirectoryW Lib "kernel32" (ByVal lPathName As Long) As Long
Private Declare Function SetPriorityClass Lib "kernel32" (ByVal lHandle As Long, ByVal lPriority As Long) As Long
Private Declare Function TerminateProcess Lib "kernel32" (ByVal lHandle As Long, ByVal lExitCode As Long) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal lHandle As Long, ByVal lMilliSeconds As Long) As Long

'METHODS

Public Function Process_GetCurrentProcessCommand(ByRef sOutCommand As String) As Boolean

    Dim l_Pointer As Long

    l_Pointer = GetCommandLineW

    If l_Pointer Then

        sOutCommand = String_CopyFromPointer(l_Pointer)

        Process_GetCurrentProcessCommand = True

    End If

End Function

Public Function Process_GetCurrentProcessDirectory(ByRef sOutDirectory As String) As Boolean

    Dim l_Length As Long
    Dim l_Temp As String

    l_Temp = Space$(MAX_PATH)
    l_Length = GetCurrentDirectoryW(MAX_PATH, StrPtr(l_Temp))

    If l_Length Then

        sOutDirectory = Left$(l_Temp, l_Length)

        Process_GetCurrentProcessDirectory = True

    End If

End Function

Public Function Process_GetProcessModule(ByVal lHandle As Long, ByRef sOutModule As String) As Boolean

    Dim l_Length As Long
    Dim l_Temp As String

    l_Temp = Space$(MAX_PATH)
    l_Length = GetModuleFileNameExW(lHandle, 0&, StrPtr(l_Temp), MAX_PATH)

    If l_Length Then

        sOutModule = Left$(l_Temp, l_Length)

        Process_GetProcessModule = True

    End If

End Function

Public Function Process_GetProcessPriority(ByVal lHandle As Long) As ENUM_PROCESS_PRIORITY

    Process_GetProcessPriority = GetPriorityClass(lHandle)

End Function

Public Function Process_IsWoW(ByVal lHandle As Long, ByRef bIsWoW As Boolean) As Boolean

    Dim l_IsWoW As Long

    If IsWow64Process(lHandle, l_IsWoW) Then

        bIsWoW = l_IsWoW

        Process_IsWoW = True

    End If

End Function

Public Function Process_SetCurrentDirectory(ByRef sPath As String) As Boolean

    Process_SetCurrentDirectory = SetCurrentDirectoryW(StrPtr(sPath))

End Function

Public Function Process_SetProcessPriority(ByVal lHandle As Long, ByVal lPriority As ENUM_PROCESS_PRIORITY) As Boolean

    Process_SetProcessPriority = SetPriorityClass(lHandle, lPriority)

End Function

Public Function Process_StartProcess(ByRef sFilePath As String, Optional ByRef sCurrentDir As String, Optional ByVal lWaitTime As Long, Optional ByVal lProcessPriority As ENUM_PROCESS_PRIORITY = NORMAL_PRIORITY_CLASS, Optional lShowWindow As ENUM_WINDOW_SHOW = SW_SHOWNORMAL) As Long

    Dim l_StartupInfo As TSTARTUPINFO
    Dim l_ProcessInfo As TPROCESSINFO
    Dim l_Result As Long

    With l_StartupInfo
        .lSize = Len(l_StartupInfo)
        .lFlags = 1&
        .iShowWindow = lShowWindow
    End With

    l_Result = CreateProcessW(StrPtr(sFilePath), StrPtr(sCurrentDir), 0&, 0&, 0&, lProcessPriority, 0&, StrPtr(sCurrentDir), VarPtr(l_StartupInfo), l_ProcessInfo)

    If l_Result Then

        If lWaitTime <> 0& Then

            If lWaitTime < 0& Then l_Result = -1& Else l_Result = lWaitTime

            If WaitForSingleObject(l_ProcessInfo.lProcess, l_Result) = -1& Then Exit Function

        End If

        Process_StartProcess = l_ProcessInfo.lProcess

    End If

End Function

Public Function Process_TerminateProcess(ByVal lHandle As Long, Optional ByVal lExitCode As Long) As Boolean

    Process_TerminateProcess = TerminateProcess(lHandle, lExitCode)

End Function
