Attribute VB_Name = "Z__WinAPI_Guid"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Shared\Core\Enums\Ole.bas
'  Shared\Core\Types\Guid.bas
'  Shared\Core\Memory.Alloc.bas
'  Shared\String.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Private Declare Function CLSIDFromProgID Lib "ole32" (ByVal lIdPointer As Long, ByRef uGuid As TGUID) As Long
Private Declare Function CLSIDFromString Lib "ole32" (ByVal lIdPointer As Long, ByRef uGuid As TGUID) As Long
Private Declare Function ProgIDFromCLSID Lib "ole32" (ByRef uGuid As TGUID, ByRef lIdPointer As Long) As Long
Private Declare Function StringFromCLSID Lib "ole32" (ByRef uGuid As TGUID, ByRef lIdPointer As Long) As Long
Private Declare Sub CoCreateGuid Lib "ole32" (ByRef uGuid As TGUID)

'METHODS

Public Function Guid_ConvertGuidToProgId(ByRef uGuid As TGUID, ByRef sOutProgId As String) As ENUM_OLE_STATUS

    Dim l_OleStatus As ENUM_OLE_STATUS
    Dim l_Pointer As Long

    l_OleStatus = ProgIDFromCLSID(uGuid, l_Pointer)

    If l_OleStatus = S_OK Then Guid_ConvertGuidToProgId = GetIdFromPointer(l_Pointer, sOutProgId) Else Guid_ConvertGuidToProgId = l_OleStatus

End Function

Public Function Guid_ConvertGuidToStringId(ByRef uGuid As TGUID, ByRef sOutStringId As String) As ENUM_OLE_STATUS

    Dim l_OleStatus As ENUM_OLE_STATUS
    Dim l_Pointer As Long

    l_OleStatus = StringFromCLSID(uGuid, l_Pointer)

    If l_OleStatus = S_OK Then Guid_ConvertGuidToStringId = GetIdFromPointer(l_Pointer, sOutStringId) Else Guid_ConvertGuidToStringId = l_OleStatus

End Function

Public Function Guid_ConvertProgIdToGuid(ByRef sProgId As String, ByRef uOutGuid As TGUID) As ENUM_OLE_STATUS

    Guid_ConvertProgIdToGuid = CLSIDFromProgID(StrPtr(sProgId), uOutGuid)

End Function

Public Function Guid_ConvertStringIdToGuid(ByRef sStringId As String, ByRef uOutGuid As TGUID) As ENUM_OLE_STATUS

    Guid_ConvertStringIdToGuid = CLSIDFromString(StrPtr(sStringId), uOutGuid)

End Function

Public Function Guid_Generate() As TGUID

    Dim l_Guid As TGUID

    CoCreateGuid l_Guid

    Guid_Generate = l_Guid

End Function

Public Function Guid_GetIDispatch() As TGUID

    With Guid_GetIDispatch 'IDispatch {00020400-0000-0000-C000-000000000046}
        .lData1 = 132096
        .bData4(0) = 192&
        .bData4(7) = 70&
    End With

End Function

Public Function Guid_GetIPicture() As TGUID

    With Guid_GetIPicture 'IPicture {7BF80980-BF32-101A-8BBB-00AA00300CAB}
        .lData1 = 2079852928
        .iData2 = -16590
        .iData3 = 4122
        .bData4(0) = 139
        .bData4(1) = 187
        .bData4(3) = 170
        .bData4(5) = 48
        .bData4(6) = 12
        .bData4(7) = 171
    End With

End Function

Public Function Guid_GetIPictureDisp() As TGUID

    With Guid_GetIPictureDisp 'IPictureDisp {7BF80981-BF32-101A-8BBB-00AA00300CAB}
        .lData1 = 2079852929
        .iData2 = -16590
        .iData3 = 4122
        .bData4(0) = 139
        .bData4(1) = 187
        .bData4(3) = 170
        .bData4(5) = 48
        .bData4(6) = 12
        .bData4(7) = 171
    End With

End Function

Public Function Guid_GetIUnknown() As TGUID

    With Guid_GetIUnknown 'IUnknown {00000000-0000-0000-C000-000000000046}
        .bData4(0) = 192&
        .bData4(7) = 70&
    End With

End Function

Private Function GetIdFromPointer(ByVal lPointer As Long, ByRef sOutId As String) As ENUM_OLE_STATUS

    Dim l_Temp As String

    l_Temp = String_CopyFromPointer(lPointer)

    If l_Temp <> vbNullString Then sOutId = l_Temp Else GetIdFromPointer = E_POINTER

    Memory_ComFree lPointer

End Function
