Attribute VB_Name = "Z__WinAPI_Dialog_File"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Shared\Core\Constants\FileSystem.bas
'  Shared\Core\Memory.bas
'  Shared\Core\String.bas
'  Shared\Core\Window.Messaging.bas
'  Shared\FileSystem.Convert.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_DIALOG_BROWSEDIR
    BIF_FILEJUNCTIONS& = 65536
    BIF_FORCOMPUTER& = 4096&
    BIF_FORPRINTER& = 8192&
    BIF_INCLUDEFILES& = 16384&
    BIF_INCLUDEURLS& = 128&
    BIF_DONTGOBELOWDOMAIN& = 2&
    BIF_EDITBOX& = 16&
    BIF_NEWDIALOGSTYLE& = 64&
    BIF_NONEWFOLDERBUTTON& = 512&
    BIF_NOTRANSLATETARGETS& = 1024&
    BIF_RETURNFSANCESTORS& = 8&
    BIF_RETURNONLYFSDIRS& = 1&
    BIF_SHAREABLE& = 32768
    BIF_STATUSTEXT& = 4&
    BIF_UAHINT& = 256&
    BIF_USENEWUI& = BIF_NEWDIALOGSTYLE&
    BIF_VALIDATE& = 32&
End Enum

Public Enum ENUM_DIALOG_BROWSEFILE
    OFN_ALLOWMULTISELECT& = 512&
    OFN_CREATEPROMPT& = 8192&
    OFN_DONTADDTORECENT& = 33554432
    OFN_ENABLEHOOK& = 32&
    OFN_ENABLEINCLUDENOTIFY& = 4194304
    OFN_ENABLESIZING& = 8388608
    OFN_ENABLETEMPLATE& = 64&
    OFN_ENABLETEMPLATEHANDLE& = 128&
    OFN_EXPLORER& = 524288
    OFN_EXTENSIONDIFFERENT& = 1024&
    OFN_FILEMUSTEXIST& = 4096&
    OFN_FORCESHOWHIDDEN& = 268435456
    OFN_HIDEREADONLY& = 4&
    OFN_LONGNAMES& = 2097152
    OFN_NOCHANGEDIR& = 8&
    OFN_NODEREFERENCELINKS& = 1048576
    OFN_NOLONGNAMES& = 262144
    OFN_NONETWORKBUTTON& = 131072
    OFN_NOREADONLYRETURN& = 32768
    OFN_NOTESTFILECREATE& = 65536
    OFN_NOVALIDATE& = 256&
    OFN_OVERWRITEPROMPT& = 2&
    OFN_PATHMUSTEXIST& = 2048&
    OFN_READONLY& = 1&
    OFN_SHAREAWARE& = 16384&
    OFN_SHAREFALLTHROUGH& = 2&
    OFN_SHAREWARN& = 0&
    OFN_SHOWHELP& = 16&
End Enum

'CONSTANTS

Private Const BFFM_INITIALIZED As Long = 1&
Private Const BFFM_SETSELECTIONW As Long = 1127&

'TYPES

Private Type TBROWSEINFO
    lhWnd As Long
    lFunc As Long
    lDispName As Long
    sTitle As String
    lFlags As Long
    lPoint As Long
    lParam As String
    lImage As Long
End Type

Private Type TOPENFILENAME
    lSize As Long
    lhWnd As Long
    lhInstance As Long
    sFilter As String
    sCustomFilter As Long
    lCustomFilterMax As Long
    lFilterIndex As Long
    sFile As String
    lFileMax As Long
    sFileTitle As String
    lFileTitleMax As Long
    sInitialDir As String
    sTitle As String
    lFlags As Long
    iFileOffset As Integer
    iFileExtension As Integer
    sDefExt As String
    lCustData As Long
    lHook As Long
    sTemplateName As Long
End Type

'DECLARATIONS

Private Declare Function GetOpenFileNameW Lib "comdlg32" (ByVal uOpenFileDlg As Long) As Long
Private Declare Function GetSaveFileNameW Lib "comdlg32" (ByVal uSaveFileDlg As Long) As Long
Private Declare Function SHBrowseForFolderW Lib "shell32" (ByVal uFolderBrws As Long) As Long
Private Declare Sub Sleep Lib "kernel32" (ByVal lMilliSeconds As Long)

'METHODS

Public Function Dialog_BrowseDir(Optional ByVal lFlags As ENUM_DIALOG_BROWSEDIR, Optional ByRef sStartFolder As String, Optional ByRef sTitle As String, Optional ByVal lhWnd As Long) As String

    Dim l_BrowseInfo As TBROWSEINFO
    Dim l_Path As String
    Dim l_Result As Long

    With l_BrowseInfo
        .lhWnd = lhWnd
        .sTitle = sTitle
        .lFlags = lFlags
        .lParam = sStartFolder
        .lPoint = Memory_CopyByRef4(AddressOf CallbackDirBrowse)
    End With

    l_Result = SHBrowseForFolderW(VarPtr(l_BrowseInfo))

    If FileSystem_ConvertIdToPath(l_Result, l_Path) Then

        If (lFlags And BIF_INCLUDEFILES) <> BIF_INCLUDEFILES Then

            If AscW(Mid$(l_Path, Len(l_Path), 1&)) <> 92 Then l_Path = l_Path & CHAR_SLASH

        End If

        Dialog_BrowseDir = l_Path

    End If

End Function

Public Function Dialog_BrowseFile(Optional ByVal bSave As Boolean, Optional ByVal lFlags As ENUM_DIALOG_BROWSEFILE, Optional ByRef sStartFolder As String, Optional ByRef sFilter As String, Optional ByRef sDefaultExt As String, Optional ByRef sTitle As String, Optional ByVal lhWnd As Long) As String

    Dim l_OpenFileName As TOPENFILENAME
    Dim l_Result As Long
    Dim l_Temp1 As String
    Dim l_Temp2 As String

    l_Temp1 = Space$(MAX_PATH_EX - 1&)
    l_Temp2 = Space$(MAX_PATH - 1&)

    With l_OpenFileName
        .lSize = Len(l_OpenFileName)
        .lhWnd = lhWnd
        .lFlags = lFlags
        .sFilter = Replace(sFilter, CHAR_VBAR, vbNullChar) & vbNullChar & vbNullChar
        .sDefExt = sDefaultExt & vbNullChar
        .lFileMax = MAX_PATH_EX
        .sFile = Space$(MAX_PATH_EX - 1&)
        .lFileTitleMax = MAX_PATH
        .sFileTitle = Space$(MAX_PATH - 1&)
        .sInitialDir = sStartFolder
        .sTitle = sTitle
    End With

    If bSave Then l_Result = GetSaveFileNameW(VarPtr(l_OpenFileName)) Else l_Result = GetOpenFileNameW(VarPtr(l_OpenFileName))

    If l_Result Then

        If (lFlags And OFN_ALLOWMULTISELECT) = OFN_ALLOWMULTISELECT Then

            l_Temp2 = Right$(l_OpenFileName.sFile, Len(l_OpenFileName.sFile) - l_OpenFileName.iFileOffset)
            l_Temp2 = Left$(l_Temp2, InStrRev(l_Temp2, vbNullChar) - 2&)
            l_Temp1 = Left$(l_OpenFileName.sFile, l_OpenFileName.iFileOffset)
            l_Temp1 = Left$(l_Temp1, String_GetLengthBeforeNullChar(l_Temp1))

            If AscW(Mid$(l_Temp1, Len(l_Temp1), 1&)) <> 92 Then l_Temp1 = l_Temp1 & CHAR_SLASH

            Dialog_BrowseFile = l_Temp1 & Replace$(l_Temp2, vbNullChar, CHAR_VBAR & l_Temp1)

        Else

            Dialog_BrowseFile = Left$(l_OpenFileName.sFile, String_GetLengthBeforeNullChar(l_OpenFileName.sFile))

        End If

    End If

End Function

Private Function CallbackDirBrowse(ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lParam As Long, ByVal lData As Long) As Long

    If lData > 0& And lMsg = BFFM_INITIALIZED Then

        Window_Notify lhWnd, BFFM_SETSELECTIONW, BFFM_INITIALIZED, lData

        Sleep 100&

        Window_NotifyAsync lhWnd, BFFM_SETSELECTIONW, BFFM_INITIALIZED, lData

    End If

End Function
