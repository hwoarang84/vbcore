Attribute VB_Name = "Z__WinAPI_System"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Shared\Core\Constants\FileSystem.bas
'  Shared\Core\String.bas
'  Shared\FileSystem.Convert.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_SYSTEM_PATH
    CSIDL_ADMINTOOLS& = 48&
    CSIDL_ALTSTARTUP& = 29&
    CSIDL_APPDATA& = 26&
    CSIDL_BITBUCKET& = 10&
    CSIDL_CDBURN_AREA& = 59&
    CSIDL_COMMON_ADMINTOOLS& = 47&
    CSIDL_COMMON_ALTSTARTUP& = 30&
    CSIDL_COMMON_APPDATA& = 35&
    CSIDL_COMMON_DESKTOPDIRECTORY& = 25&
    CSIDL_COMMON_DOCUMENTS& = 46&
    CSIDL_COMMON_FAVORITES& = 31&
    CSIDL_COMMON_MUSIC& = 53&
    CSIDL_COMMON_OEM_LINKS& = 58&
    CSIDL_COMMON_PICTURES& = 54&
    CSIDL_COMMON_PROGRAMS& = 23&
    CSIDL_COMMON_STARTMENU& = 22&
    CSIDL_COMMON_STARTUP& = 24&
    CSIDL_COMMON_TEMPLATES& = 45&
    CSIDL_COMMON_VIDEO& = 55&
    CSIDL_COMPUTERSNEARME& = 61&
    CSIDL_CONNECTIONS& = 49&
    CSIDL_CONTROLS& = 3&
    CSIDL_COOKIES& = 33&
    CSIDL_DESKTOP& = 0&
    CSIDL_DESKTOPDIRECTORY& = 16&
    CSIDL_DRIVES& = 17&
    CSIDL_FAVORITES& = 6&
    CSIDL_FONTS& = 20&
    CSIDL_HISTORY& = 34&
    CSIDL_INTERNET& = 1&
    CSIDL_INTERNET_CACHE& = 32&
    CSIDL_LOCAL_APPDATA& = 28&
    CSIDL_MYDOCUMENTS& = 12&
    CSIDL_MYMUSIC& = 13&
    CSIDL_MYPICTURES& = 39&
    CSIDL_MYVIDEO& = 14&
    CSIDL_NETHOOD& = 19&
    CSIDL_NETWORK& = 18&
    CSIDL_PERSONAL& = 5&
    CSIDL_PHOTOALBUMS& = 69&
    CSIDL_PLAYLISTS& = 63&
    CSIDL_PRINTERS& = 4&
    CSIDL_PRINTHOOD& = 27&
    CSIDL_PROFILE& = 40&
    CSIDL_PROGRAM_FILES& = 38&
    CSIDL_PROGRAM_FILESX86& = 42&
    CSIDL_PROGRAM_FILES_COMMON& = 43&
    CSIDL_PROGRAM_FILES_COMMONX86& = 44&
    CSIDL_PROGRAMS& = 2&
    CSIDL_RECENT& = 8&
    CSIDL_RESOURCES& = 56&
    CSIDL_RESOURCES_LOCALIZED& = 57&
    CSIDL_SAMPLE_MUSIC& = 64&
    CSIDL_SAMPLE_PICTURES& = 66&
    CSIDL_SAMPLE_PLAYLISTS& = 65&
    CSIDL_SAMPLE_VIDEOS& = 67&
    CSIDL_SENDTO& = 9&
    CSIDL_STARTMENU& = 11&
    CSIDL_STARTUP& = 7&
    CSIDL_SYSTEM& = 37&
    CSIDL_SYSTEMX86& = 41&
    CSIDL_TEMP& = 71&
    CSIDL_TEMPLATES& = 21&
    CSIDL_WINDOWS& = 36&
End Enum

'DECLARATIONS

Private Declare Function GetSystemDirectoryW Lib "kernel32" (ByVal lBuffer As Long, ByVal lBufferLen As Long) As Long
Private Declare Function GetTempPathW Lib "kernel32" (ByVal lBufferLen As Long, ByVal lBuffer As Long) As Long
Private Declare Function GetWindowsDirectoryW Lib "kernel32" (ByVal lBuffer As Long, ByVal lBufferLen As Long) As Long
Private Declare Function SHGetSpecialFolderLocation Lib "shell32" (ByVal lhWnd As Long, ByVal lFolder As Long, ByRef lListId As Long) As Long

'METHODS

Public Function System_GetPath(ByVal lId As ENUM_SYSTEM_PATH, ByRef sOutPath As String) As Boolean

    Dim l_Result As Long
    Dim l_Path As String

    Select Case lId

        Case CSIDL_WINDOWS

            l_Path = Space$(MAX_PATH)
            l_Result = GetWindowsDirectoryW(StrPtr(l_Path), MAX_PATH)

        Case CSIDL_TEMP

            l_Path = Space$(MAX_PATH)
            l_Result = GetTempPathW(MAX_PATH, StrPtr(l_Path))

        Case CSIDL_SYSTEM, CSIDL_SYSTEMX86

            l_Path = Space$(MAX_PATH)
            l_Result = GetSystemDirectoryW(StrPtr(l_Path), MAX_PATH)

        Case Else

            If SHGetSpecialFolderLocation(0&, lId, l_Result) = 0& Then l_Result = FileSystem_ConvertIdToPath(l_Result, l_Path)

    End Select

    If l_Result Then

        If l_Result < 0& Then l_Result = Len(l_Path) Else l_Result = String_GetLengthBeforeNullChar(l_Path)

        If AscW(Mid$(l_Path, l_Result, 1&)) <> 92 Then sOutPath = Left$(l_Path, l_Result) & CHAR_SLASH Else sOutPath = Left$(l_Path, l_Result)

        System_GetPath = True

    End If

End Function
