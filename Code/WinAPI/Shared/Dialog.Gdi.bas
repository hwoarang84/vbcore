Attribute VB_Name = "Z___WinAPI_Shared_Dialog_Gdi"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Shared\Core\Gdi.bas
'  Shared\Core\Memory.bas
'  Shared\Core\Memory.Alloc.bas
'  Shared\Core\String.bas
'  Shared\Core\Window.Dc.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_DIALOG_COLORBOX
    CC_ANYCOLOR& = 256&
    CC_ENABLEHOOK& = 16&
    CC_ENABLETEMPLATE& = 32&
    CC_ENABLETEMPLATEHANDLE& = 64&
    CC_FULLOPEN& = 2&
    CC_PREVENTFULLOPEN& = 4&
    CC_RGBINIT& = 1&
    CC_SHOWHELP& = 8&
    CC_SOLIDCOLOR& = 128&
End Enum

Public Enum ENUM_DIALOG_FONTBOX
    CF_ANSIONLY& = 1024&
    CF_APPLY& = 512&
    CF_BOTH& = 3&
    CF_EFFECTS& = 256&
    CF_ENABLEHOOK& = 8&
    CF_ENABLETEMPLATE& = 16&
    CF_ENABLETEMPLATEHANDLE& = 32&
    CF_FIXEDPITCHONLY& = 16384&
    CF_FORCEFONTEXIST& = 65536
    CF_INACTIVEFONTS& = 33554432
    CF_INITTOLOGFONTSTRUCT& = 64&
    CF_LIMITSIZE& = 8192&
    CF_NOFACESEL& = 524288
    CF_NOOEMFONTS& = 2048&
    CF_NOSCRIPTSEL& = 8388608
    CF_NOSIMULATIONS& = 4096&
    CF_NOSIZESEL& = 2097152
    CF_NOSTYLESEL& = 1048576
    CF_NOVECTORFONTS& = 2048&
    CF_NOVERTFONTS& = 16777216
    CF_PRINTERFONTS& = 2&
    CF_SCALABLEONLY& = 131072
    CF_SCREENFONTS& = 1&
    CF_SELECTSCRIPT& = 4194304
    CF_SHOWHELP& = 4&
    CF_TTONLY& = 262144
    CF_USESTYLE& = 128&
    CF_WYSIWYG& = 32768
End Enum

'TYPES

Private Type TCHOOSECOLOR
    lSize As Long
    lhWnd As Long
    lhInstance As Long
    lRgbResult As Long
    lCustColors As Long
    lFlags As Long
    lCustData As Long
    lHook As Long
    sTemplateName As Long
End Type

Private Type TCHOOSEFONT
    lSize As Long
    lhWnd As Long
    lHdc As Long
    lLogFont As Long
    lPointSize As Long
    lFlags As Long
    lRgbColors As Long
    lCustData As Long
    lHook As Long
    sTemplateName As Long
    lhInstance As Long
    sStyle As Long
    iFontType As Integer
    iMissingAlign As Integer
    lSizeMin As Long
    lSizeMax As Long
End Type

Private Type TLOGFONT
    lHeight As Long
    lWidth As Long
    lEscapement As Long
    lOrientation As Long
    lWeight As Long
    bItalic As Byte
    bUnderline As Byte
    bStrikeOut As Byte
    bCharSet As Byte
    bOutPrecision As Byte
    bClipPrecision As Byte
    bQuality As Byte
    bPitchAndFamily As Byte
    sFaceName(63) As Byte
End Type

'CONSTANTS

Private Const SIZE_MULTIPLIER As Long = 10&

'DECLARATIONS

Private Declare Function ChooseColorW Lib "comdlg32" (ByVal uColorChoose As Long) As Long
Private Declare Function ChooseFontW Lib "comdlg32" (ByVal uFontChoose As Long) As Long

'METHODS

Public Function Dialog_ColorBox(ByRef lInOutRgbColor As Long, Optional ByVal lFlags As ENUM_DIALOG_COLORBOX, Optional ByVal lhWnd As Long) As Boolean

    Dim l_ChooseColor As TCHOOSECOLOR
    Dim l_CustomColors(15) As Long

    With l_ChooseColor
        .lSize = Len(l_ChooseColor)
        .lCustColors = VarPtr(l_CustomColors(0))
        .lhWnd = lhWnd
        .lFlags = lFlags
        .lRgbResult = lInOutRgbColor
    End With

    If ChooseColorW(VarPtr(l_ChooseColor)) Then

        lInOutRgbColor = l_ChooseColor.lRgbResult

        Dialog_ColorBox = True

    End If

End Function

Public Function Dialog_FontBox( _
   ByRef sName As String, ByRef lInOutSize As Long, ByRef lInOutWeight As Long, ByRef bInOutItal As Boolean, ByRef bInOutUndl As Boolean, ByRef bInOutStrk As Boolean, ByRef lInOutColor As Long, _
   Optional ByRef lInOutCharSet As Long, Optional ByVal lInOutPres As Long, Optional ByVal lClipPres As Long, Optional ByVal lQuality As ENUM_GDI_FONTQUALITY, Optional ByVal lPitch As Long, _
   Optional ByVal lFlags As ENUM_DIALOG_FONTBOX, Optional ByVal lhInstance As Long, Optional ByVal lhWnd As Long) As Boolean

    Dim l_LogFont As TLOGFONT
    Dim l_LogFontLength As Long
    Dim l_LogFontPointer As Long
    Dim l_ChooseFont As TCHOOSEFONT
    Dim l_Memory As Long
    Dim l_Result As Long

    l_Result = Window_GetDC(0&)

    With l_LogFont
        .bCharSet = lInOutCharSet
        .bClipPrecision = lClipPres
        .bOutPrecision = lInOutPres
        .bItalic = -bInOutItal
        .bQuality = lQuality
        .bUnderline = -bInOutUndl
        .bStrikeOut = -bInOutStrk
        .lHeight = Gdi_MulDiv(l_Result, lInOutSize)
        .bPitchAndFamily = lPitch
        .lWeight = lInOutWeight
    End With

    Window_ReleaseDC 0&, l_Result

    Memory_Copy l_LogFont.sFaceName(0), ByVal StrPtr(sName), Len(sName) * 2&

    l_LogFontLength = Len(l_LogFont)
    l_Memory = Memory_GlobalAlloc(GHND, l_LogFontLength)
    l_LogFontPointer = Memory_GlobalLock(l_Memory)

    Memory_Copy ByVal l_LogFontPointer, l_LogFont, l_LogFontLength

    With l_ChooseFont
        .lSize = Len(l_ChooseFont)
        .lhWnd = lhWnd
        .lFlags = lFlags
        .lhInstance = lhInstance
        .lPointSize = lInOutSize * SIZE_MULTIPLIER
        .lLogFont = l_LogFontPointer
        .lRgbColors = lInOutColor
    End With

    If ChooseFontW(VarPtr(l_ChooseFont)) Then

        Memory_Copy l_LogFont, ByVal l_LogFontPointer, l_LogFontLength

        sName = Left$(l_LogFont.sFaceName, String_GetLengthBeforeNullChar((l_LogFont.sFaceName)))
        lInOutCharSet = l_LogFont.bCharSet
        lInOutColor = l_ChooseFont.lRgbColors
        lInOutSize = l_ChooseFont.lPointSize \ SIZE_MULTIPLIER
        bInOutItal = l_LogFont.bItalic
        bInOutUndl = l_LogFont.bUnderline
        bInOutStrk = l_LogFont.bStrikeOut
        lInOutWeight = l_LogFont.lWeight

        Dialog_FontBox = True

    End If

    Memory_GlobalUnlock l_Memory
    Memory_GlobalFree l_Memory

End Function
