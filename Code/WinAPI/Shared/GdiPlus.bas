Attribute VB_Name = "Z___WinAPI_Shared_GdiPlus"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Core\Types\Gdi.bas
'  Core\Gdi.Convert.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_GDIPLUS_BRUSHSTYLE
    BrushTypeHatchFill& = 1&
    BrushTypeLinearGradient& = 4&
    BrushTypePathGradient& = 3&
    BrushTypeSolidColor& = 0&
    BrushTypeTextureFill& = 2&
End Enum

Public Enum ENUM_GDIPLUS_DASHSTYLE
    DashStyleCustom& = 5&
    DashStyleDash& = 1&
    DashStyleDashDot& = 3&
    DashStyleDashDotDot& = 4&
    DashStyleDot& = 2&
    DashStyleSolid& = 0&
End Enum

Public Enum ENUM_GDIPLUS_SMOOTHINGMODE
    SmoothingModeAntiAlias& = 4&
    SmoothingModeAntiAlias8x4& = 5&
    SmoothingModeAntiAlias8x8& = 6&
    SmoothingModeDefault& = 0&
    SmoothingModeInvalid& = -1&
    SmoothingModeHighSpeed& = 1&
    SmoothingModeHighQuality& = 2&
    SmoothingModeNone& = 3&
End Enum

Public Enum ENUM_GDIPLUS_STATUS
    Aborted& = 9&
    AccessDenied& = 12&
    FileNotFound& = 10&
    FontFamilyNotFound& = 14&
    FontStyleNotFound& = 15&
    GdiplusNotInitialized& = 18&
    GenericError& = 1&
    InsufficientBuffer& = 5&
    InvalidParameter& = 2&
    NotImplemented& = 6&
    NotTrueTypeFont& = 16&
    ObjectBusy& = 4&
    Ok& = 0&
    OutOfMemory& = 3&
    ProfileNotFound& = 21&
    PropertyNotFound& = 19&
    PropertyNotSupported& = 20&
    UnknownImageFormat& = 13&
    UnsupportedGdiplusVersion& = 17&
    ValueOverflow& = 11&
    Win32Error& = 7&
    WrongState& = 8&
End Enum

'CONSTANTS

Private Const GDIPLUS_VERSION As Long = 1&

'DECLARATIONS

Private Declare Function GdipCreateFromHDC Lib "gdiplus" (ByVal lHdc As Long, ByRef lGraphics As Long) As Long
Private Declare Function GdipCreateLineBrushFromRectI Lib "gdiplus" (ByRef uRect As TRECT, ByVal lColor1 As Long, ByVal lColor2 As Long, ByVal lMode As Long, ByVal lWrapMode As Long, ByRef lBrush As Long) As Long
Private Declare Function GdipCreatePen1 Lib "gdiplus" (ByVal lColor As Long, ByVal lWidth As Single, ByVal lUnit As Long, ByRef lPen As Long) As Long
Private Declare Function GdipDeleteBrush Lib "gdiplus" (ByVal lBrush As Long) As Long
Private Declare Function GdipDeleteGraphics Lib "gdiplus" (ByVal lGraphics As Long) As Long
Private Declare Function GdipDeletePen Lib "gdiplus" (ByVal lPen As Long) As Long
Private Declare Function GdipDisposeImage Lib "gdiplus" (ByVal lImage As Long) As Long
Private Declare Function GdipDrawImageRectI Lib "gdiplus" (ByVal lGraphics As Long, ByVal lImage As Long, ByVal lX As Long, ByVal lY As Long, ByVal lWidth As Long, ByVal lHeight As Long) As Long
Private Declare Function GdipDrawImageRectRectI Lib "gdiplus" (ByVal lGraphics As Long, ByVal lImage As Long, ByVal lDstX As Long, ByVal lDstY As Long, ByVal lDstWidth As Long, ByVal lDstHeight As Long, ByVal lSrcX As Long, ByVal lSrcY As Long, ByVal lSrcWidth As Long, ByVal lSrcHeight As Long, ByVal lSrcUnit As Long, ByVal lImageAttributes As Long, ByVal lCallback As Long, ByVal lCallbackData As Long) As Long
Private Declare Function GdipDrawLineI Lib "gdiplus" (ByVal lGraphics As Long, ByVal lPen As Long, ByVal lX1 As Long, ByVal lY1 As Long, ByVal lX2 As Long, ByVal lY2 As Long) As Long
Private Declare Function GdipFillRectangleI Lib "gdiplus" (ByVal lGraphics As Long, ByVal lBrush As Long, ByVal lX As Long, ByVal lY As Long, ByVal lWidth As Long, ByVal lHeight As Long) As Long
Private Declare Function GdipGetImageHeight Lib "gdiplus" (ByVal lImage As Long, ByRef lHeight As Long) As Long
Private Declare Function GdipGetImageWidth Lib "gdiplus" (ByVal lImage As Long, ByRef lWidth As Long) As Long
Private Declare Function GdiplusStartup Lib "gdiplus" (ByRef lToken As Long, ByRef uInput As Any, ByVal lOutput As Long) As Long
Private Declare Function GdipLoadImageFromStream Lib "gdiplus" (ByVal lStream As Long, ByRef lImage As Long) As Long
Private Declare Function GdipSetPenDashStyle Lib "gdiplus" (ByVal lPen As Long, ByVal lStyle As Long) As Long
Private Declare Function GdipSetSmoothingMode Lib "gdiplus" (ByVal lGraphics As Long, ByVal lSmoothingMode As Long) As Long
Private Declare Sub GdiplusShutdown Lib "gdiplus" (ByVal lToken As Long)

'METHODS

Public Function GdiPlus_CreateBrush(ByVal lX As Long, ByVal lY As Long, ByVal lWidth As Long, ByVal lHeight As Long, ByVal lStartColor As Long, ByVal lEndColor As Long, ByRef lOutBrush As Long, Optional ByVal lMode As ENUM_GDIPLUS_BRUSHSTYLE) As ENUM_GDIPLUS_STATUS

    Dim l_Rect As TRECT

    With l_Rect
        .lLeft = lX
        .lTop = lY
        .lRight = lWidth
        .lBottom = lHeight
    End With

    GdiPlus_CreateBrush = GdipCreateLineBrushFromRectI(l_Rect, Gdi_ConvertRGBToRGBA(lStartColor), Gdi_ConvertRGBToRGBA(lEndColor), lMode, 0&, lOutBrush)

End Function

Public Function GdiPlus_CreateGraphics(ByVal lHdc As Long, ByRef lOutGraphics As Long) As ENUM_GDIPLUS_STATUS

    GdiPlus_CreateGraphics = GdipCreateFromHDC(lHdc, lOutGraphics)

End Function

Public Function GdiPlus_CreateImageFromStream(ByVal lStream As Long, ByRef lOutImage As Long) As ENUM_GDIPLUS_STATUS

    GdiPlus_CreateImageFromStream = GdipLoadImageFromStream(lStream, lOutImage)

End Function

Public Function GdiPlus_CreatePen(ByVal lColor As Long, ByVal lWidth As Long, ByRef lOutPen As Long, Optional ByVal lStyle As ENUM_GDIPLUS_DASHSTYLE) As ENUM_GDIPLUS_STATUS

    Dim l_Status As ENUM_GDIPLUS_STATUS

    l_Status = GdipCreatePen1(Gdi_ConvertRGBToRGBA(lColor, 255), lWidth, 2&, lOutPen)

    If l_Status = Ok And lStyle <> DashStyleSolid Then GdipSetPenDashStyle lOutPen, lStyle

    GdiPlus_CreatePen = l_Status

End Function

Public Function GdiPlus_DeleteBrush(ByVal lBrush As Long) As ENUM_GDIPLUS_STATUS

    GdiPlus_DeleteBrush = GdipDeleteBrush(lBrush)

End Function

Public Function GdiPlus_DeleteGraphics(ByVal lGraphics As Long) As ENUM_GDIPLUS_STATUS

    GdiPlus_DeleteGraphics = GdipDeleteGraphics(lGraphics)

End Function

Public Function GdiPlus_DeleteImage(ByVal lImage As Long) As ENUM_GDIPLUS_STATUS

    GdiPlus_DeleteImage = GdipDisposeImage(lImage)

End Function

Public Function GdiPlus_DeletePen(ByVal lPen As Long) As ENUM_GDIPLUS_STATUS

    GdiPlus_DeletePen = GdipDeletePen(lPen)

End Function

Public Function GdiPlus_DrawImage(ByVal lGraphics As Long, ByVal lImage As Long, ByVal lDstX As Long, ByVal lDstY As Long, ByVal lDstWidth As Long, ByVal lDstHeight As Long, Optional ByVal lSrcWidth As Long, Optional ByVal lSrcHeight As Long) As ENUM_GDIPLUS_STATUS

    If lSrcHeight > 0& And lSrcWidth > 0& Then

        GdiPlus_DrawImage = GdipDrawImageRectRectI(lGraphics, lImage, lDstX, lDstY, lDstWidth, lDstHeight, 0&, 0&, lSrcWidth, lSrcHeight, 2&, 0&, 0&, 0&)

    Else

        GdiPlus_DrawImage = GdipDrawImageRectI(lGraphics, lImage, lDstX, lDstY, lDstWidth, lDstHeight)

    End If

End Function

Public Function GdiPlus_DrawLine(ByVal lGraphics As Long, ByVal lPen As Long, ByVal lX1 As Long, ByVal lY1 As Long, ByVal lX2 As Long, ByVal lY2 As Long, Optional ByVal lSmootingMode As ENUM_GDIPLUS_SMOOTHINGMODE) As ENUM_GDIPLUS_STATUS

    Dim l_Status As ENUM_GDIPLUS_STATUS

    If lSmootingMode <> SmoothingModeDefault Then l_Status = GdipSetSmoothingMode(lGraphics, lSmootingMode)

    If l_Status = Ok Then l_Status = GdipDrawLineI(lGraphics, lPen, lX1, lY1, lX2, lY2)

    GdiPlus_DrawLine = l_Status

End Function

Public Function GdiPlus_FillRect(ByVal lGraphics As Long, ByVal lBrush As Long, ByVal lX As Long, ByVal lY As Long, ByVal lWidth As Long, ByVal lHeight As Long) As ENUM_GDIPLUS_STATUS

    GdiPlus_FillRect = GdipFillRectangleI(lGraphics, lBrush, lX, lY, lWidth, lHeight)

End Function

Public Function GdiPlus_GetImageSize(ByVal lImage As Long, ByRef lOutHeight As Long, ByRef lOutWidth As Long) As ENUM_GDIPLUS_STATUS

    Dim l_Status As ENUM_GDIPLUS_STATUS

    l_Status = GdipGetImageHeight(lImage, lOutHeight)

    If l_Status = Ok Then l_Status = GdipGetImageWidth(lImage, lOutWidth)

    GdiPlus_GetImageSize = l_Status

End Function

Public Function GdiPlus_Start(ByRef lOutToken As Long) As ENUM_GDIPLUS_STATUS

    Dim l_StartupInfo(3) As Long

    l_StartupInfo(0) = GDIPLUS_VERSION

    GdiPlus_Start = GdiplusStartup(lOutToken, l_StartupInfo(0), 0&)

End Function

Public Sub GdiPlus_Terminate(ByVal lToken As Long)

    If lToken Then GdiplusShutdown lToken

End Sub
