Attribute VB_Name = "Z___WinAPI_Shared_System_Interaction"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Core\Enums\System.bas
'  Core\Process.bas
'  Core\Process.Token.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Private Const SE_SHUTDOWN_PRIVILEGE As String = "SeShutdownPrivilege"
Private Const SHTDN_REASON_MINOR_MAINTENANCE As Long = 65535

'DECLARATIONS

Private Declare Function ExitWindowsEx Lib "user32" (ByVal lFlags As Long, ByVal lReason As Long) As Long

'METHODS

Public Function System_Exit(ByVal lFlags As ENUM_SYSTEM_EXIT) As Boolean

    Dim l_Result As Boolean
    Dim l_Token As Long

    If lFlags <> EWX_LOGOFF Then

        l_Result = Process_TokenOpen(Process_GetCurrentProcessHandle, TOKEN_ADJUST_PRIVILEGES Or TOKEN_QUERY, l_Token)

        If l_Result Then l_Result = Process_TokenAdjust(l_Token, SE_SHUTDOWN_PRIVILEGE)

    Else

        l_Result = True

    End If

    If l_Result Then System_Exit = ExitWindowsEx(lFlags, SHTDN_REASON_MINOR_MAINTENANCE)

End Function
