Attribute VB_Name = "Z___WinAPI_Shared_Gdi"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Core\Enums\Gdi.bas
'  Core\Enums\Ole.bas
'  Core\Types\Gdi.bas
'  Core\Types\Guid.bas
'  Core\Gdi.bas
'  Core\Memory.BitShift.bas
'  Core\Window.Dc.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_GDI_DASHSTYLE
    PS_DASH = 1&
    PS_DASHDOT = 3&
    PS_DASHDOTDOT = 4&
    PS_DOT = 2&
    PS_INSIDEFRAME = 6&
    PS_NULL = 5&
    PS_SOLID = 0&
End Enum

Public Enum ENUM_GDI_EDGEDRAWFORMAT
    BF_ADJUST& = 8192&
    BF_BOTTOM& = 8&
    BF_DIAGONAL& = 16&
    BF_FLAT& = 16384&
    BF_LEFT& = 1&
    BF_MIDDLE& = 2048&
    BF_MONO& = 32768
    BF_RIGHT& = 4&
    BF_SOFT& = 4096&
    BF_TOP& = 2&
    BF_BOTTOMLEFT& = (BF_BOTTOM Or BF_LEFT)
    BF_BOTTOMRIGHT& = (BF_BOTTOM Or BF_RIGHT)
    BF_DIAGONAL_ENDBOTTOMLEFT& = (BF_DIAGONAL Or BF_BOTTOM Or BF_LEFT)
    BF_DIAGONAL_ENDBOTTOMRIGHT& = (BF_DIAGONAL Or BF_BOTTOM Or BF_RIGHT)
    BF_DIAGONAL_ENDTOPLEFT& = (BF_DIAGONAL Or BF_TOP Or BF_LEFT)
    BF_DIAGONAL_ENDTOPRIGHT& = (BF_DIAGONAL Or BF_TOP Or BF_RIGHT)
    BF_RECT& = (BF_LEFT Or BF_TOP Or BF_RIGHT Or BF_BOTTOM)
    BF_TOPLEFT& = (BF_TOP Or BF_LEFT)
    BF_TOPRIGHT& = (BF_TOP Or BF_RIGHT)
End Enum

Public Enum ENUM_GDI_EDGETYPE
    BDR_RAISEDINNER& = 4&
    BDR_RAISEDOUTER& = 1&
    BDR_SUNKENINNER& = 8&
    BDR_SUNKENOUTER& = 2&
    BDR_BUMP& = BDR_RAISEDOUTER Or BDR_SUNKENINNER
    BDR_ETCHED& = BDR_RAISEDINNER Or BDR_SUNKENOUTER
    BDR_RAISED& = BDR_RAISEDINNER Or BDR_RAISEDOUTER
    BDR_SUNKEN& = BDR_SUNKENINNER Or BDR_SUNKENOUTER
End Enum

Public Enum ENUM_GDI_PICTURETYPE
    PICTYPE_BITMAP& = 1&
    PICTYPE_ENHMETAFILE& = 4&
    PICTYPE_ICON& = 3&
    PICTYPE_METAFILE& = 2&
    PICTYPE_NONE& = 0&
    PICTYPE_UNINITIALIZED& = -1&
End Enum

Public Enum ENUM_GDI_TEXTDRAWFORMAT
    DT_BOTTOM& = 8&
    DT_CALCRECT& = 1024&
    DT_CENTER& = 1&
    DT_EDITCONTROL& = 8192&
    DT_END_ELLIPSIS& = 32768
    DT_EXPANDTABS& = 64&
    DT_EXTERNALLEADING& = 512&
    DT_HIDEPREFIX& = 1048576
    DT_INTERNAL& = 4096&
    DT_LEFT& = 0&
    DT_MODIFYSTRING& = 65536
    DT_NOCLIP& = 256&
    DT_NOFULLWIDTHCHARBREAK& = 524288
    DT_NOPREFIX& = 2048&
    DT_PATH_ELLIPSIS& = 16384&
    DT_PREFIXONLY& = 2097152
    DT_RIGHT& = 2&
    DT_RTLREADING& = 131072
    DT_SINGLELINE& = 32&
    DT_TABSTOP& = 128&
    DT_TOP& = 0&
    DT_VCENTER& = 4&
    DT_WORDBREAK& = 16&
    DT_WORD_ELLIPSIS& = 262144
End Enum

'TYPES

Private Type TBITMAP
    lType As Long
    lWidth As Long
    lHeight As Long
    lWidthB As Long
    iPlanes As Integer
    iBitsPixel As Integer
    lBits As Long
End Type

Private Type TBITMAPINDIRECT
    lSize As Long
    lType As Long
    lhBmp As Long
    lhPal As Long
    lReserved As Long
End Type

'CONSTANTS

Private Const HALFTONE As Long = 4&
Private Const Opacity As Byte = 255

'DECLARATIONS

Public Declare Function Gdi_CreateSolidBrush Lib "gdi32" Alias "CreateSolidBrush" (ByVal lColor As Long) As Long
Public Declare Function Gdi_SelectObject Lib "gdi32" Alias "SelectObject" (ByVal lHdc As Long, ByVal lObject As Long) As Long
Public Declare Function Gdi_SetBackColor Lib "gdi32" Alias "SetBkColor" (ByVal lHdc As Long, ByVal lColor As Long) As Long
Public Declare Function Gdi_SetTextColor Lib "gdi32" Alias "SetTextColor" (ByVal lHdc As Long, ByVal lColor As Long) As Long

Private Declare Function CreateBitmap Lib "gdi32" (ByVal lWidth As Long, ByVal lHeight As Long, ByVal lPlanes As Long, ByVal lBitCount As Long, ByRef uBits As Any) As Long
Private Declare Function CreateCompatibleDC Lib "gdi32" (ByVal lHdc As Long) As Long
Private Declare Function CreateFontW Lib "gdi32" (ByVal lHeight As Long, ByVal lWidth As Long, ByVal lEsc As Long, ByVal lOrient As Long, ByVal lWeight As Long, ByVal lItal As Long, ByVal lUndl As Long, ByVal lStrk As Long, ByVal lCharSet As Long, ByVal lOutPres As Long, ByVal lClipPres As Long, ByVal lQual As Long, ByVal lPitch As Long, ByVal lFont As Long) As Long
Private Declare Function CreatePen Lib "gdi32" (ByVal lStyle As Long, ByVal lWidth As Long, ByVal lColor As Long) As Long
Private Declare Function CreateStreamOnHGlobal Lib "ole32" (ByVal lHGlobal As Long, ByVal lDeleteOnRelease As Long, ByRef uUnknown As Any) As Long
Private Declare Function DeleteDC Lib "gdi32" (ByVal lHdc As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal lObject As Long) As Long
Private Declare Function DrawEdge Lib "user32" (ByVal lHdc As Long, uRect As TRECT, ByVal lEdge As Long, ByVal lFlags As Long) As Long
Private Declare Function DrawTextW Lib "user32" (ByVal lHdc As Long, ByVal lString As Long, ByVal lLen As Long, uRect As TRECT, ByVal lFormat As Long) As Long
Private Declare Function GdiAlphaBlend Lib "gdi32" (ByVal lHdcDst As Long, ByVal lXNew As Long, ByVal lYNew As Long, ByVal lWidthNew As Long, ByVal lHeightNew As Long, ByVal lHdcSrc As Long, ByVal lXSrc As Long, ByVal lYSrc As Long, ByVal lWidthSrc As Long, ByVal lHeightSrc As Long, ByVal lBlend As Long) As Long
Private Declare Function GetObjectA Lib "gdi32" (ByVal lObject As Long, ByVal lBytes As Long, ByRef uBitmap As TBITMAP) As Long
Private Declare Function LineTo Lib "gdi32" (ByVal lHdc As Long, ByVal lX As Long, ByVal lY As Long) As Long
Private Declare Function MoveToEx Lib "gdi32" (ByVal lHdc As Long, ByVal lX As Long, ByVal lY As Long, ByVal lPoint As Long) As Long
Private Declare Function OleCreatePictureIndirect Lib "oleaut32" (ByRef uPicDesc As TBITMAPINDIRECT, ByRef uGuid As TGUID, ByVal lPicOwnsHandle As Long, ByRef uPicture As Any) As Long
Private Declare Function OleLoadPicture Lib "oleaut32" (ByVal lStream As Long, ByVal lSize As Long, ByVal lRunMode As Long, ByRef uGuid As TGUID, ByRef uPicture As Any) As Long
Private Declare Function OleTranslateColor Lib "oleaut32" (ByVal lOleColor As Long, ByVal lPalette As Long, ByRef lColorRef As Long) As Long
Private Declare Function SetStretchBltMode Lib "gdi32" (ByVal lHdc As Long, ByVal lMode As Long) As Long
Private Declare Function StretchBlt Lib "gdi32" (ByVal lHdc As Long, ByVal lXNew As Long, ByVal lYNew As Long, ByVal lWidthNew As Long, ByVal lHeightNew As Long, ByVal lHdcSrc As Long, ByVal lXSrc As Long, ByVal lYSrc As Long, ByVal lWidthSrc As Long, ByVal lHeightSrc As Long, ByVal lMethod As Long) As Long

'METHODS

Public Function Gdi_ConvertOleColorToRGB(ByVal lOleColor As Long, ByRef lOutRgbColor As Long, Optional ByVal lPalette As Long) As ENUM_OLE_STATUS

    Gdi_ConvertOleColorToRGB = OleTranslateColor(lOleColor, lPalette, lOutRgbColor)

End Function

Public Function Gdi_CreateBitmap(ByVal lWidth As Long, ByVal lHeight As Long, ByVal lPlanes As Long, ByVal lBitCount As Long) As Long

    Gdi_CreateBitmap = CreateBitmap(lWidth, lHeight, lPlanes, lBitCount, ByVal 0&)

End Function

Public Function Gdi_CreateDC(Optional ByVal lHdc As Long) As Long

    Gdi_CreateDC = CreateCompatibleDC(lHdc)

End Function

Public Function Gdi_CreateFont(ByVal lhWnd As Long, ByVal lSize As Long, ByVal lEsc As Long, ByVal lOrient As Long, ByVal lWeight As Long, ByVal bItal As Boolean, ByVal bUndl As Boolean, ByVal bStrk As Boolean, ByVal lCharSet As Long, ByVal lOutPres As Long, ByVal lClipPres As Long, ByVal lQuality As ENUM_GDI_FONTQUALITY, ByVal lPitch As Long, ByVal sName As Long) As Long

    Dim l_Dc As Long

    l_Dc = Window_GetDC(lhWnd)

    If l_Dc Then

        Gdi_CreateFont = CreateFontW(Gdi_MulDiv(l_Dc, lSize), 0&, lEsc, lOrient, lWeight, -bItal, -bUndl, -bStrk, lCharSet, lOutPres, lClipPres, lQuality, lPitch, sName)

        Window_ReleaseDC lhWnd, l_Dc

    End If

End Function

Public Function Gdi_CreatePen(ByVal lColor As Long, ByVal lWidth As Long, Optional ByVal lStyle As ENUM_GDI_DASHSTYLE) As Long

    Gdi_CreatePen = CreatePen(lStyle, lWidth, lColor)

End Function

Public Function Gdi_CreatePictureFromStream(ByVal lStream As Long, ByVal lSize As Long, ByVal lRunMode As Long, ByRef uGuid As TGUID, ByRef uOutPicture As Object) As ENUM_OLE_STATUS

    Gdi_CreatePictureFromStream = OleLoadPicture(lStream, lSize, lRunMode, uGuid, uOutPicture)

End Function

Public Function Gdi_CreatePictureIndirect(ByRef uGuid As TGUID, ByVal lBitmap As Long, ByVal lType As ENUM_GDI_PICTURETYPE, ByVal lhPal As Long, ByVal bPicOwnsHandle As Boolean, ByRef uOutPicture As Object) As ENUM_OLE_STATUS

    Dim l_Bitmap As TBITMAPINDIRECT

    With l_Bitmap
        .lSize = Len(l_Bitmap)
        .lhBmp = lBitmap
        .lhPal = lhPal
        .lType = lType
    End With

    Gdi_CreatePictureIndirect = OleCreatePictureIndirect(l_Bitmap, uGuid, -bPicOwnsHandle, uOutPicture)

End Function

Public Function Gdi_CreateStream(ByVal lHGlobal As Long, ByVal bDeleteOnRelease As Boolean, ByRef uOutStream As Object) As ENUM_OLE_STATUS

    Gdi_CreateStream = CreateStreamOnHGlobal(lHGlobal, -bDeleteOnRelease, uOutStream)

End Function

Public Function Gdi_DeleteDC(ByVal lHdc As Long) As Boolean

    Gdi_DeleteDC = DeleteDC(lHdc)

End Function

Public Function Gdi_DeleteObject(ByVal lObject As Long) As Boolean

    Gdi_DeleteObject = DeleteObject(lObject)

End Function

Public Function Gdi_DrawEdge(ByVal lHdc As Long, ByVal lLeft As Long, ByVal lTop As Long, ByVal lWidth As Long, ByVal lHeight As Long, ByVal lType As ENUM_GDI_EDGETYPE, ByVal lFormat As ENUM_GDI_EDGEDRAWFORMAT) As Boolean

    Dim l_Rect As TRECT

    With l_Rect
        .lBottom = lHeight + lTop
        .lLeft = lLeft
        .lRight = lWidth + lLeft
        .lTop = lTop
    End With

    Gdi_DrawEdge = DrawEdge(lHdc, l_Rect, lType, lFormat)

End Function

Public Function Gdi_DrawLine(ByVal lHdc As Long, ByVal lLeft As Long, ByVal lTop As Long, ByVal lRight As Long, ByVal lBottom As Long) As Boolean

    If MoveToEx(lHdc, lLeft, lTop, 0&) Then Gdi_DrawLine = LineTo(lHdc, lRight, lBottom)

End Function

Public Function Gdi_DrawImage(ByVal lSrcHdc As Long, ByVal lDestHdc As Long, ByVal lSrcWidth As Long, ByVal lSrcHeight As Long, ByVal lDestX As Long, ByVal lDestY As Long, ByVal lDestWidth As Long, ByVal lDestHeight As Long, ByVal bTransparency As Byte) As Boolean

    If bTransparency = 0 Then

        SetStretchBltMode lDestHdc, HALFTONE

        Gdi_DrawImage = StretchBlt(lDestHdc, lDestX, lDestY, lDestWidth, lDestHeight, lSrcHdc, 0&, 0&, lSrcWidth, lSrcHeight, vbSrcCopy)

    Else

        Gdi_DrawImage = GdiAlphaBlend(lDestHdc, lDestX, lDestY, lDestWidth, lDestHeight, lSrcHdc, 0&, 0&, lSrcWidth, lSrcHeight, Memory_Set32HighWord(0&, Opacity - bTransparency))

    End If

End Function

Public Function Gdi_DrawText(ByVal lHdc As Long, ByVal sText As Long, ByVal lLeft As Long, ByVal lTop As Long, ByRef lInOutWidth As Long, ByRef lInOutHeight As Long, ByVal lFormat As ENUM_GDI_TEXTDRAWFORMAT) As Long

    Dim l_Length As Long
    Dim l_Rect As TRECT
    Dim l_Result As Long

    l_Length = Len(sText)

    With l_Rect
        .lBottom = lInOutHeight + lTop
        .lLeft = lLeft
        .lRight = lInOutWidth + lLeft
        .lTop = lTop
    End With

    l_Result = DrawTextW(lHdc, sText, l_Length, l_Rect, lFormat)

    If l_Result Then

        If (lFormat And DT_CALCRECT) = DT_CALCRECT Then

            lInOutHeight = l_Rect.lBottom
            lInOutWidth = l_Rect.lRight

        End If

        Gdi_DrawText = l_Result

    End If

End Function

Public Function Gdi_GetImageInfo(ByVal lImage As Long, ByRef lOutHeight As Long, ByRef lOutWidth As Long, ByRef lOutBitsPixel As Long) As Boolean

    Dim l_Bitmap As TBITMAP

    If GetObjectA(lImage, Len(l_Bitmap), l_Bitmap) Then

        lOutHeight = l_Bitmap.lHeight
        lOutWidth = l_Bitmap.lWidth
        lOutBitsPixel = l_Bitmap.iBitsPixel

        Gdi_GetImageInfo = True

    End If

End Function
