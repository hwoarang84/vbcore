Attribute VB_Name = "Z___WinAPI_Shared_FileSystem_Convert"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Core\Constants\FileSystem.bas
'  Core\String.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Private Declare Function SHGetPathFromIDListW Lib "shell32" (ByVal lListId As Long, ByVal lRootPath As Long) As Long

'METHODS

Public Function FileSystem_ConvertIdToPath(ByVal lListId As Long, ByRef sOutPath As String) As Boolean

    Dim l_Temp As String

    If lListId Then

        l_Temp = Space$(MAX_PATH)

        If SHGetPathFromIDListW(lListId, StrPtr(l_Temp)) Then

            sOutPath = Left$(l_Temp, String_GetLengthBeforeNullChar(l_Temp))

            FileSystem_ConvertIdToPath = True

        End If

    End If

End Function
