Attribute VB_Name = "Z___WinAPI_Shared_String"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Core\Memory.bas
'  Core\String.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'METHODS

Public Function String_CopyFromPointer(ByVal sSource As Long, Optional ByVal lSourceLen As Long, Optional ByVal lSourceMaxLen As Long) As String

    If lSourceLen = 0& Then lSourceLen = String_GetCharsCount(sSource)

    If lSourceLen Then

        If lSourceLen > lSourceMaxLen And lSourceMaxLen > 0& Then lSourceLen = lSourceMaxLen

        String_CopyFromPointer = Space$(lSourceLen)

        Memory_CopyFast lSourceLen * 2&, ByVal StrPtr(String_CopyFromPointer), ByVal sSource

    End If

End Function
