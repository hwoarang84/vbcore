Attribute VB_Name = "Z____WinAPI_Shared_Core_Process"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Public Declare Function Process_GetCurrentProcessHandle Lib "kernel32" Alias "GetCurrentProcess" () As Long
Public Declare Function Process_GetCurrentProcessId Lib "kernel32" Alias "GetCurrentProcessId" () As Long
