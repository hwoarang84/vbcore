Attribute VB_Name = "Z____WinAPI_Shared_Core_Dialog"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_DIALOG_MESSAGEBOX
    MB_ABORTRETRYIGNORE& = 2&
    MB_CANCELTRYCONTINUE& = 6&
    MB_DEFAULT_DESKTOP_ONLY& = 131072
    MB_DEFBUTTON2& = 256&
    MB_DEFBUTTON3& = 512&
    MB_DEFBUTTON4& = 768&
    MB_DEFMASK& = 3840&
    MB_HELP& = 16384&
    MB_ICONEXCLAMATION& = 48&
    MB_ICONINFORMATION& = 64&
    MB_ICONQUESTION& = 32&
    MB_ICONSTOP& = 16&
    MB_MISCMASK& = 49152
    MB_MODEMASK& = 12288&
    MB_NOFOCUS& = 32768
    MB_OK& = 0&
    MB_OKCANCEL& = 1&
    MB_RETRYCANCEL& = 5&
    MB_RIGHT& = 524288
    MB_RTLREADING& = 1048576
    MB_SERVICE_NOTIFICATION& = 2097152
    MB_SETFOREGROUND& = 65536
    MB_SYSTEMMODAL& = 4096&
    MB_TASKMODAL& = 8192&
    MB_TOPMOST& = 262144
    MB_TYPEMASK& = 15&
    MB_USERICON& = 128&
    MB_YESNO& = 4&
    MB_YESNOCANCEL& = 3&
End Enum

Public Enum ENUM_DIALOG_MESSAGEBOX_RESULT
    IDABORT& = 3&
    IDCANCEL& = 2&
    IDCONTINUE& = 11&
    IDIGNORE& = 5&
    IDNO& = 7&
    IDOK& = 1&
    IDRETRY& = 4&
    IDTRYAGAIN& = 10&
    IDYES& = 6&
End Enum

'CONSTANTS

Private Const MAX_TEXT_LENGTH As Long = 3072&
Private Const MAX_TITLE_LENGTH As Long = 200&

'DECLARATIONS

Private Declare Function MessageBoxW Lib "user32" (ByVal lhWnd As Long, ByVal lMessage As Long, ByVal lCaption As Long, ByVal lType As Long) As Long

'METHODS

Public Function Dialog_MessageBox(ByRef sText As String, Optional ByVal lFlags As ENUM_DIALOG_MESSAGEBOX, Optional ByRef sTitle As String, Optional ByVal lhWnd As Long) As ENUM_DIALOG_MESSAGEBOX_RESULT

    Dialog_MessageBox = MessageBoxW(lhWnd, StrPtr(Left$(sText, MAX_TEXT_LENGTH) & vbNullChar), StrPtr(Left$(sTitle, MAX_TITLE_LENGTH) & vbNullChar), lFlags)

End Function
