Attribute VB_Name = "Z____WinAPI_Shared_Core_Process_Library"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Private Declare Function GetModuleHandleW Lib "kernel32" (ByVal sName As Long) As Long
Private Declare Function FreeLibrary Lib "kernel32" (ByVal lModule As Long) As Long
Private Declare Function LoadLibraryW Lib "kernel32" (ByVal sName As Long) As Long

'METHODS

Public Function Process_GetLoadedLibraryHandle(ByRef sName As String) As Long

    Process_GetLoadedLibraryHandle = GetModuleHandleW(StrPtr(sName))

End Function

Public Function Process_LibraryLoad(ByRef sName As String) As Long

    Process_LibraryLoad = LoadLibraryW(StrPtr(sName))

End Function

Public Function Process_LibraryRelease(ByVal lHandle As Long) As Boolean

    Process_LibraryRelease = FreeLibrary(lHandle)

End Function
