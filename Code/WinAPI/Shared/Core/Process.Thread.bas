Attribute VB_Name = "Z____WinAPI_Shared_Core_Process_Thread"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_THREAD_PRIORITY
    THREAD_PRIORITY_ABOVE_NORMAL& = 1&
    THREAD_PRIORITY_BELOW_NORMAL& = -1&
    THREAD_PRIORITY_HIGHEST& = 2&
    THREAD_PRIORITY_IDLE& = -15&
    THREAD_PRIORITY_LOWEST& = -2&
    THREAD_PRIORITY_NORMAL& = 0&
    THREAD_PRIORITY_TIME_CRITICAL& = 15&
End Enum

'TYPES

Private Type TMSG
    lhWnd As Long
    lMessage As Long
    lwParam As Long
    lParam As Long
    lTime As Long
    lPtX As Long
    lPtY As Long
End Type

'CONSTANTS

Private Const WAIT_IO_COMPLETION As Long = 192&

'DECLARATIONS

Public Declare Function Process_GetCurrentThreadHandle Lib "kernel32" Alias "GetCurrentThread" () As Long
Public Declare Function Process_GetCurrentThreadId Lib "kernel32" Alias "GetCurrentThreadId" () As Long
Public Declare Function Process_GetCurrentThreadLocale Lib "kernel32" Alias "GetThreadLocale" () As Long
Public Declare Function Process_GetThreadId Lib "kernel32" Alias "GetThreadId" (ByVal lHandle As Long) As Long

Private Declare Function DispatchMessageA Lib "user32" (ByRef uMsg As TMSG) As Long
Private Declare Function GetThreadPriority Lib "kernel32" (ByVal lHandle As Long) As Long
Private Declare Function PeekMessageA Lib "user32" (ByRef uMsg As TMSG, ByVal lhWnd As Long, ByVal lFilterMin As Long, ByVal lFilterMax As Long, ByVal lRemoveMsg As Long) As Long
Private Declare Function SleepEx Lib "kernel32" (ByVal lMilliSeconds As Long, ByVal lAlertable As Long) As Long
Private Declare Function SetThreadPriority Lib "kernel32" (ByVal lHandle As Long, ByVal lPriority As Long) As Long
Private Declare Function TerminateThread Lib "kernel32" (ByVal lHandle As Long, ByVal lExitCode As Long) As Long
Private Declare Function TranslateMessage Lib "user32" (ByRef uMsg As TMSG) As Long

'METHODS

Public Sub Process_DoEvents()

    Dim l_Msg As TMSG

    Do While PeekMessageA(l_Msg, 0&, 0&, 0&, 1&)
        TranslateMessage l_Msg
        DispatchMessageA l_Msg
    Loop

End Sub

Public Function Process_GetThreadPriority(ByVal lHandle As Long) As ENUM_THREAD_PRIORITY

    Process_GetThreadPriority = GetThreadPriority(lHandle)

End Function

Public Function Process_SetThreadPriority(ByVal lHandle As Long, ByVal lPriority As ENUM_THREAD_PRIORITY) As Boolean

    Process_SetThreadPriority = SetThreadPriority(lHandle, lPriority)

End Function

Public Function Process_SuspendCurrentThread(ByVal lMilliSeconds As Long, Optional ByVal bIsAlertable As Boolean) As Boolean

    Dim l_Value As Long

    l_Value = SleepEx(lMilliSeconds, -bIsAlertable)

    Process_SuspendCurrentThread = (l_Value = 0& Or l_Value = WAIT_IO_COMPLETION)

End Function

Public Function Process_TerminateThread(ByVal lHandle As Long, Optional ByVal lExitCode As Long) As Boolean

    Process_TerminateThread = TerminateThread(lHandle, lExitCode)

End Function
