Attribute VB_Name = "Z____WinAPI_Shared_Core_System_Version"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'TYPES

Public Type TOSVERSIONINFOEX
    lSize As Long
    lVerMajor As Long
    lVerMinor As Long
    lBuild As Long
    lPlatform As Long
    sSPVersion As String * 128
    iSPMajor As Integer
    iSPMinor As Integer
    iSuiteMask As Integer
    bProductType As Byte
    bReserved As Byte
End Type

'DECLARATIONS

Private Declare Function GetVersionExW Lib "kernel32" (ByVal uOSVersion As Long) As Long

'METHODS

Public Function System_GetVersion(ByRef uOutVersionInfo As TOSVERSIONINFOEX) As Boolean

    uOutVersionInfo.lSize = LenB(uOutVersionInfo)

    System_GetVersion = GetVersionExW(VarPtr(uOutVersionInfo))

End Function
