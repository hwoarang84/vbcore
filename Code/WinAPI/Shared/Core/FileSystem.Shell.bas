Attribute VB_Name = "Z____WinAPI_Shared_Core_FileSystem_Shell"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Constants\FileSystem.bas
'  Enums\Window.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_FILESYSTEM_EXECVERB
    EXECVERB_EDIT& = 1&
    EXECVERB_EXPLORE& = 2&
    EXECVERB_FIND& = 3&
    EXECVERB_NULL& = 0&
    EXECVERB_OPEN& = 4&
    EXECVERB_PRINT& = 5&
    EXECVERB_PROPERTIES& = 6&
    EXECVERB_RUNAS& = 7&
End Enum

Public Enum ENUM_FILESYSTEM_EXECSTATUS
    ERROR_BAD_FORMAT& = 11&
    ERROR_FILE_NOT_FOUND& = 2&
    ERROR_PATH_NOT_FOUND& = 3&
    SE_ERR_ACCESSDENIED& = 5&
    SE_ERR_ASSOCINCOMPLETE& = 27&
    SE_ERR_DDEBUSY& = 30&
    SE_ERR_DDEFAIL& = 29&
    SE_ERR_DDETIMEOUT& = 28&
    SE_ERR_DLLNOTFOUND& = 32&
    SE_ERR_NOASSOC& = 31&
    SE_ERR_OOM& = 8&
    SE_ERR_SHARE& = 26&
End Enum

Public Enum ENUM_FILESYSTEM_OPERATION
    FO_COPY& = 2&
    FO_DELETE& = 3&
    FO_MOVE& = 1&
    FO_RENAME& = 4&
End Enum

Public Enum ENUM_FILESYSTEM_OPERATION_FLAG
    FOF_ALLOWUNDO& = 64&
    FOF_CONFIRMMOUSE& = 2&
    FOF_FILESONLY& = 128&
    FOF_MULTIDESTFILES& = 1&
    FOF_NO_CONNECTED_ELEMENTS& = 8192&
    FOF_NOCONFIRMATION& = 16&
    FOF_NOCONFIRMMKDIR& = 512&
    FOF_NOCOPYSECURITYATTRIBS& = 2048&
    FOF_NOERRORUI& = 1024&
    FOF_NORECURSEREPARSE& = 32768
    FOF_NORECURSION& = 4096&
    FOF_RENAMEONCOLLISION& = 8&
    FOF_SILENT& = 4&
    FOF_SIMPLEPROGRESS& = 256&
    FOF_WANTMAPPINGHANDLE& = 32&
    FOF_WANTNUKEWARNING& = 16384&
End Enum

'CONSTANTS

Private Const EXEC_VERB_EDIT As String = "edit"
Private Const EXEC_VERB_EXPLORE As String = "explore"
Private Const EXEC_VERB_FIND As String = "find"
Private Const EXEC_VERB_OPEN As String = "open"
Private Const EXEC_VERB_PRINT As String = "print"
Private Const EXEC_VERB_PROPERTIES As String = "properties"
Private Const EXEC_VERB_RUNAS As String = "runas"

'TYPES

Private Type TFILEOPERATION
    lhWnd As Long
    lFunc As Long
    sFrom As String
    sTo As String
    iFlags As Integer
    lAborted As Long
    lNameMaps As Long
    sProgress As String
End Type

'DECLARATIONS

Private Declare Function ShellExecuteW Lib "shell32" (ByVal lhWnd As Long, ByVal lOperation As Long, ByVal lFile As Long, ByVal lParameters As Long, ByVal lDirectory As Long, ByVal lShowCmd As Long) As Long
Private Declare Function SHFileOperationW Lib "shell32" (ByVal uFileOper As Long) As Long

'METHODS

Public Function FileSystem_Execute(ByRef sPath As String, ByVal lOperation As ENUM_FILESYSTEM_EXECVERB, Optional ByRef sDirectory As String, Optional ByRef sParameters As String, Optional ByVal lShowWindow As ENUM_WINDOW_SHOW = SW_SHOWNORMAL, Optional ByVal lhWnd As Long) As ENUM_FILESYSTEM_EXECSTATUS

    Dim l_Verb As String

    Select Case lOperation

        Case EXECVERB_EDIT: l_Verb = EXEC_VERB_EDIT

        Case EXECVERB_EXPLORE: l_Verb = EXEC_VERB_EXPLORE

        Case EXECVERB_FIND: l_Verb = EXEC_VERB_FIND

        Case EXECVERB_OPEN: l_Verb = EXEC_VERB_OPEN

        Case EXECVERB_PRINT: l_Verb = EXEC_VERB_PRINT

        Case EXECVERB_PROPERTIES: l_Verb = EXEC_VERB_PROPERTIES

        Case EXECVERB_RUNAS: l_Verb = EXEC_VERB_RUNAS

    End Select

    FileSystem_Execute = ShellExecuteW(lhWnd, StrPtr(l_Verb), StrPtr(sPath), StrPtr(sParameters), StrPtr(sDirectory), lShowWindow)

End Function

Public Function FileSystem_Operation(ByVal lOperation As ENUM_FILESYSTEM_OPERATION, ByRef sSourcePath As String, Optional ByRef sTargetPath As String, Optional ByVal lFlags As ENUM_FILESYSTEM_OPERATION_FLAG, Optional ByVal lhWnd As Long) As Boolean

    Dim l_FileOperation As TFILEOPERATION
    Dim l_Result As Long

    With l_FileOperation
        .lhWnd = lhWnd
        .lFunc = lOperation
        .iFlags = lFlags
        .sFrom = Replace(sSourcePath, CHAR_VBAR, vbNullChar) & vbNullChar & vbNullChar
        .sTo = Replace(sTargetPath, CHAR_VBAR, vbNullChar) & vbNullChar & vbNullChar
    End With

    l_Result = SHFileOperationW(VarPtr(l_FileOperation))

    FileSystem_Operation = l_Result = 0& And l_FileOperation.lAborted = 0&

End Function
