Attribute VB_Name = "Z____WinAPI_Shared_Core_System_Registry"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_SYSTEM_REGISTRY_ACCESS
    KEY_ALL_ACCESS& = 983103
    KEY_CREATE_LINK& = 32&
    KEY_CREATE_SUB_KEY& = 4&
    KEY_ENUMERATE_SUB_KEYS& = 8&
    KEY_NOTIFY& = 16&
    KEY_QUERY_VALUE& = 1&
    KEY_READ& = 131097
    KEY_EXECUTE& = KEY_READ
    KEY_SET_VALUE& = 2&
    KEY_WOW64_32KEY& = 512&
    KEY_WOW64_64KEY = 256&
    KEY_WRITE& = 131078
End Enum

Public Enum ENUM_SYSTEM_REGISTRY_HIVE
    HKEY_CLASSES_ROOT& = &H80000000
    HKEY_CURRENT_CONFIG& = &H80000005
    HKEY_CURRENT_USER& = &H80000001
    HKEY_CURRENT_USER_LOCAL_SETTINGS& = &H80000007
    HKEY_DYN_DATA& = &H80000006
    HKEY_LOCAL_MACHINE& = &H80000002
    HKEY_PERFORMANCE_DATA& = &H80000004
    HKEY_PERFORMANCE_NLSTEXT& = &H80000060
    HKEY_PERFORMANCE_TEXT& = &H80000050
    HKEY_USERS& = &H80000003
End Enum

'DECLARATIONS

Public Declare Function System_RegistryKeyClose Lib "advapi32" Alias "RegCloseKey" (ByVal lHandle As Long) As Long

Private Declare Function RegCreateKeyExW Lib "advapi32" (ByVal lHKey As Long, ByVal lSubKey As Long, ByVal lReserved As Long, ByVal lClass As Long, ByVal lOptions As Long, ByVal lFlags As Long, ByVal uSecurityAttributes As Long, ByRef lResult As Long, ByVal lDisposition As Long) As Long
Private Declare Function RegOpenKeyExW Lib "advapi32" (ByVal lHKey As Long, ByVal lSubKey As Long, ByVal lOptions As Long, ByVal lDesired As Long, ByRef lResult As Long) As Long

'METHODS

Public Function System_RegistryKeyOpen(ByVal lHive As ENUM_SYSTEM_REGISTRY_HIVE, ByVal lAccess As ENUM_SYSTEM_REGISTRY_ACCESS, ByRef sPath As String, ByRef lHandle As Long, Optional ByVal bCreateIfNotExists As Boolean) As Long

    Dim l_Handle As Long
    Dim l_Result As Long

    If bCreateIfNotExists Then l_Result = RegCreateKeyExW(lHive, StrPtr(sPath), 0&, 0&, 0&, lAccess, 0&, l_Handle, 0&) Else l_Result = RegOpenKeyExW(lHive, sPath, 0&, lAccess, l_Handle)

    If l_Result = 0& Then lHandle = l_Handle Else System_RegistryKeyOpen = l_Result

End Function
