Attribute VB_Name = "Z____WinAPI_Shared_Core_Gdi_Convert"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Private Const LONG_AVG As Long = &H1000000 '16777216
Private Const LONG_MIN As Long = &H80000000 '-2147483648

'METHODS

Public Function Gdi_ConvertRGBToRGBA(ByVal lRgbColor As Long, Optional ByVal bOpacity As Byte = 255) As Long

    Dim l_Color As Long

    l_Color = (lRgbColor And &HFF&) * &H10000 Or ((lRgbColor And 65535) \ &H100&) * &H100& Or ((lRgbColor And &HFFFFFF) \ &H10000)

    If bOpacity > 127 Then Gdi_ConvertRGBToRGBA = l_Color Or ((bOpacity - 128) * LONG_AVG) Or LONG_MIN Else Gdi_ConvertRGBToRGBA = l_Color Or (bOpacity * LONG_AVG)

End Function
