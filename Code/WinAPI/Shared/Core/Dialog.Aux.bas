Attribute VB_Name = "Z____WinAPI_Shared_Core_Dialog_Aux"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_DIALOG_HELPBOX
    HH_ALINK_LOOKUP& = 19&
    HH_CLOSE_ALL& = 18&
    HH_DISPLAY_INDEX& = 2&
    HH_DISPLAY_SEARCH& = 3&
    HH_DISPLAY_TEXT_POPUP& = 14&
    HH_DISPLAY_TOC& = 1&
    HH_DISPLAY_TOPIC& = 0&
    HH_GET_LAST_ERROR& = 20&
    HH_GET_WIN_HANDLE& = 6&
    HH_GET_WIN_TYPE& = 5&
    HH_INITIALIZE& = 28&
    HH_HELP_CONTEXT& = 15&
    HH_KEYWORD_LOOKUP& = 13&
    HH_PRETRANSLATEMESSAGE& = 253&
    HH_SET_WIN_TYPE& = 4&
    HH_SYNC& = 9&
    HH_TP_HELP_CONTEXTMENU& = 16&
    HH_TP_HELP_WM_HELP& = 17&
    HH_UNINITIALIZE& = 29&
End Enum

'CONSTANTS

Private Const TOPIC_DIV As String = ">"

'DECLARATIONS

Private Declare Function HtmlHelpW Lib "hhctrl.ocx" (ByVal lhWnd As Long, ByVal lFile As Long, ByVal lCommand As Long, ByVal lData As Long) As Long

'METHODS

Public Function Dialog_HelpBox(ByRef sPath As String, Optional ByVal lFlags As ENUM_DIALOG_HELPBOX, Optional ByRef sTopic As String, Optional ByVal lCommand As Long, Optional ByVal lhWnd As Long) As Boolean

    If (lFlags And HH_DISPLAY_TOPIC) = HH_DISPLAY_TOPIC And Len(sTopic) > 0& Then

        Dialog_HelpBox = HtmlHelpW(lhWnd, StrPtr(sPath & TOPIC_DIV & sTopic), lFlags, 0&)

    Else

        Dialog_HelpBox = HtmlHelpW(lhWnd, sPath, lFlags, lCommand)

    End If

End Function
