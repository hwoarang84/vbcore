Attribute VB_Name = "Z____WinAPI_Shared_Core_DateTime"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Types\DateTime.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Public Declare Sub DateTime_GetLocal Lib "kernel32" Alias "GetLocalTime" (ByRef uOutTime As TSYSTEMTIME)
Public Declare Sub DateTime_GetSystem Lib "kernel32" Alias "GetSystemTime" (ByRef uOutTime As TSYSTEMTIME)
