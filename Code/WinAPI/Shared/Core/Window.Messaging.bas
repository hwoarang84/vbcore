Attribute VB_Name = "Z____WinAPI_Shared_Core_Window_Messaging"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Public Declare Function Window_CallProc Lib "user32" Alias "CallWindowProcW" (ByVal lPrevWndFunc As Long, ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lParam1 As Long, ByVal lParam2 As Long) As Long
Public Declare Function Window_Notify Lib "user32" Alias "SendMessageW" (ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lParam1 As Long, ByVal lParam2 As Long) As Long
Public Declare Function Window_NotifyAsync Lib "user32" Alias "PostMessageW" (ByVal lhWnd As Long, ByVal lMsg As Long, ByVal lParam1 As Long, ByVal lParam2 As Long) As Long
