Attribute VB_Name = "Z____WinAPI_Shared_Core_Memory_BitShift"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Private Const INT_MAX As Long = &H7FFF '32767
Private Const INT_MAX_OVERFLOWED As Long = &H8000& '32768
Private Const INT_MAX_UNSIGNED As Long = &H10000 '65536
Private Const INT_MAX_UNSIGNED_SHIFTED As Long = &HFFFF& '65535
Private Const LONG_MIN As Long = &H80000000 '-2147483648

'METHODS

Public Function Memory_Get32HighWord(ByVal lValue As Long) As Integer

    Memory_Get32HighWord = ((lValue And &H7FFF0000) \ INT_MAX_UNSIGNED) Or (INT_MAX_OVERFLOWED And (lValue < 0))

End Function

Public Function Memory_Get32LowWord(ByVal lValue As Long) As Integer

    Memory_Get32LowWord = (lValue And INT_MAX) Or (INT_MAX_OVERFLOWED And ((lValue And INT_MAX_OVERFLOWED) = INT_MAX_OVERFLOWED))

End Function

Public Function Memory_Set32HighWord(ByVal lValue As Long, ByVal iWord As Integer) As Long

    Memory_Set32HighWord = (lValue And INT_MAX_UNSIGNED_SHIFTED) Or ((iWord And INT_MAX) * INT_MAX_UNSIGNED) Or (LONG_MIN And (iWord < 0))

End Function

Public Function Memory_Set32LowWord(ByVal lValue As Long, ByVal iWord As Integer) As Long

    Memory_Set32LowWord = (lValue And &HFFFF0000) Or (iWord And INT_MAX_UNSIGNED_SHIFTED)

End Function
