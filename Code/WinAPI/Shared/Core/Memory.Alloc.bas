Attribute VB_Name = "Z____WinAPI_Shared_Core_Memory_Alloc"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_MEMORY_ALLOC
    GMEM_FIXED& = 0&
    GMEM_MOVEABLE& = 2&
    GMEM_ZEROINIT& = 64&
    GHND& = GMEM_MOVEABLE Or GMEM_ZEROINIT
End Enum

'DECLARATIONS

Public Declare Function Memory_ComAlloc Lib "ole32" Alias "CoTaskMemAlloc" (ByVal lSize As Long) As Long
Public Declare Function Memory_GlobalFree Lib "kernel32" Alias "GlobalFree" (ByVal lAddress As Long) As Long
Public Declare Function Memory_GlobalLock Lib "kernel32" Alias "GlobalLock" (ByVal lAddress As Long) As Long
Public Declare Function Memory_GlobalUnlock Lib "kernel32" Alias "GlobalUnlock" (ByVal lAddress As Long) As Long
Public Declare Sub Memory_ComFree Lib "ole32" Alias "CoTaskMemFree" (ByVal lAddress As Long)

Private Declare Function GlobalAlloc Lib "kernel32" (ByVal lFlags As Long, ByVal lBytes As Long) As Long

'METHODS

Public Function Memory_GlobalAlloc(ByVal lFlags As ENUM_MEMORY_ALLOC, ByVal lBytes As Long) As Long

    Memory_GlobalAlloc = GlobalAlloc(lFlags, lBytes)

End Function
