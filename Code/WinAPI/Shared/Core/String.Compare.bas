Attribute VB_Name = "Z____WinAPI_Shared_Core_String_Compare"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Private Const CSTR_OFFSET As Long = 2&

'DECLARATIONS

Private Declare Function CompareStringW Lib "kernel32" (ByVal lLocale As Long, ByVal lFlags As Long, ByVal lString1 As Long, ByVal lCount1 As Long, ByVal lString2 As Long, ByVal lCount2 As Long) As Long

'METHODS

Public Function String_Compare(ByVal lCompareMethod As Long, ByVal sString1 As Long, ByVal lStringLen1 As Long, ByVal sString2 As Long, ByVal lStringLen2 As Long) As Long

    Dim l_Result As Long

    l_Result = CompareStringW(0&, -(lCompareMethod > 0&), sString1, lStringLen1, sString2, lStringLen2)

    If l_Result Then String_Compare = l_Result - CSTR_OFFSET

End Function
