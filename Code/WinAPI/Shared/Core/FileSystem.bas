Attribute VB_Name = "Z____WinAPI_Shared_Core_FileSystem"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Constants\FileSystem.bas
'  Types\DateTime.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_FILESYSTEM_ACCESS
    FILE_ADD_FILE& = 2&
    FILE_ADD_SUBDIRECTORY& = 4&
    FILE_APPEND_DATA& = FILE_ADD_SUBDIRECTORY&
    FILE_CREATE_PIPE_INSTANCE& = FILE_ADD_SUBDIRECTORY&
    FILE_DELETE_CHILD& = 64&
    FILE_EXECUTE& = 32&
    FILE_GENERIC_ALL& = 268435456
    FILE_GENERIC_EXECUTE& = 536870912
    FILE_GENERIC_READ& = &H80000000
    FILE_GENERIC_WRITE& = 1073741824
    FILE_LIST_DIRECTORY& = 1&
    FILE_READ_ATTRIBUTES& = 128&
    FILE_READ_CONTROL& = 131072
    FILE_READ_DATA& = FILE_LIST_DIRECTORY&
    FILE_READ_EA& = 8&
    FILE_STANDARD_RIGHTS_READ& = FILE_READ_CONTROL&
    FILE_STANDARD_RIGHTS_REQUIRED& = 983040
    FILE_STANDARD_RIGHTS_WRITE& = FILE_READ_CONTROL&
    FILE_SYNCHRONIZE& = 1048576
    FILE_TRAVERSE& = FILE_EXECUTE&
    FILE_WRITE_ATTRIBUTES& = 256&
    FILE_WRITE_DATA& = FILE_ADD_FILE&
    FILE_WRITE_EA& = 16&
    FILE_ALL_ACCESS = FILE_STANDARD_RIGHTS_REQUIRED& Or FILE_SYNCHRONIZE& Or 511&
End Enum

Public Enum ENUM_FILESYSTEM_ATTRIBUTES
    FILE_ATTRIBUTE_ARCHIVE& = 32&
    FILE_ATTRIBUTE_COMPRESSED& = 2048&
    FILE_ATTRIBUTE_DEVICE& = 64&
    FILE_ATTRIBUTE_DIRECTORY& = 16&
    FILE_ATTRIBUTE_ENCRYPTED& = 16384&
    FILE_ATTRIBUTE_HIDDEN& = 2&
    FILE_ATTRIBUTE_INTEGRITY_STREAM& = 32768
    FILE_ATTRIBUTE_NORMAL& = 128&
    FILE_ATTRIBUTE_NOT_CONTENT_INDEXED& = 8192&
    FILE_ATTRIBUTE_NO_SCRUB_DATA& = 131072
    FILE_ATTRIBUTE_OFFLINE& = 4096&
    FILE_ATTRIBUTE_READONLY& = 1&
    FILE_ATTRIBUTE_RECALL_ON_DATA_ACCESS& = 4194304
    FILE_ATTRIBUTE_RECALL_ON_OPEN& = 262144
    FILE_ATTRIBUTE_REPARSE_POINT& = 1024&
    FILE_ATTRIBUTE_SPARSE_FILE& = 512&
    FILE_ATTRIBUTE_SYSTEM& = 4&
    FILE_ATTRIBUTE_TEMPORARY& = 256&
    FILE_ATTRIBUTE_VIRTUAL& = 65536
    FILE_ATTRIBUTE_PINNED& = 524288
    FILE_ATTRIBUTE_UNPINNED& = 1048576
End Enum

Public Enum ENUM_FILESYSTEM_CREATION
    CREATE_ALWAYS& = 2&
    CREATE_NEW& = 1&
    OPEN_ALWAYS& = 4&
    OPEN_EXISTING& = 3&
    TRUNCATE_EXISTING& = 5&
End Enum

Public Enum ENUM_FILESYSTEM_FLAGS
    FILE_FLAG_BACKUP_SEMANTICS& = 33554432
    FILE_FLAG_DELETE_ON_CLOSE& = 67108864
    FILE_FLAG_NO_BUFFERING& = 536870912
    FILE_FLAG_OPEN_NO_RECALL& = 1048576
    FILE_FLAG_OPEN_REPARSE_POINT& = 2097152
    FILE_FLAG_OVERLAPPED& = 1073741824
    FILE_FLAG_POSIX_SEMANTICS& = 16777216
    FILE_FLAG_RANDOM_ACCESS& = 268435456
    FILE_FLAG_SESSION_AWARE& = 8388608
    FILE_FLAG_SEQUENTIAL_SCAN& = 134217728
    FILE_FLAG_WRITE_THROUGH& = &H80000000
End Enum

Public Enum ENUM_FILESYSTEM_SHARE
    FILE_SHARE_DELETE& = 4&
    FILE_SHARE_NONE& = 0&
    FILE_SHARE_READ& = 1&
    FILE_SHARE_WRITE& = 2&
End Enum

'CONSTANTS

Private Const SIZE_OFFSET As Long = &H80000000

'TYPES

Public Type TFINDDATA
    lAttributes As Long
    uCreationTime As TFILETIME
    uLastAccessTime As TFILETIME
    uLastWriteTime As TFILETIME
    lSizeHigh As Long
    lSizeLow As Long
    lReserved0 As Long
    lReserved1 As Long
    sFileName As String * MAX_PATH
    sAlternate As String * 14&
End Type

Private Type TOVERLAPPED
    lInternal As Long
    lInternalHigh As Long
    lOffset As Long
    lOffsetHigh As Long
    lhEvent As Long
End Type

'DECLARATIONS

Public Declare Function FileSystem_FindClose Lib "kernel32" Alias "FindClose" (ByVal lHandle As Long) As Long

Private Declare Function CloseHandle Lib "kernel32" (ByVal lHandle As Long) As Long
Private Declare Function CreateFileW Lib "kernel32" (ByVal lName As Long, ByVal lAccessMode As Long, ByVal lShare As Long, ByVal lSecurity As Long, ByVal lCreation As Long, ByVal lFlags As Long, ByVal lTemplateFile As Long) As Long
Private Declare Function DeleteFileW Lib "kernel32" (ByVal lName As Long) As Long
Private Declare Function FindFirstFileW Lib "kernel32" (ByVal lName As Long, ByVal lFindData As Long) As Long
Private Declare Function FindNextFileW Lib "kernel32" (ByVal lHandle As Long, ByVal lFindData As Long) As Long
Private Declare Function GetFileAttributesW Lib "kernel32" (ByVal lFileName As Long) As Long
Private Declare Function GetFileSize Lib "kernel32" (ByVal lHandle As Long, lSizeHigh As Long) As Long
Private Declare Function ReadFile Lib "kernel32" (ByVal lHandle As Long, ByRef uBuffer As Any, ByVal lBytesToRead As Long, ByRef lBytesRead As Long, ByRef uOverlapped As Any) As Long
Private Declare Function SetFileAttributesW Lib "kernel32" (ByVal lFileName As Long, ByVal lFileAttributes As Long) As Long
Private Declare Function WriteFile Lib "kernel32" (ByVal lHandle As Long, ByRef uBuffer As Any, ByVal lBytesToWrite As Long, ByRef lBytesWritten As Long, ByRef uOverlapped As Any) As Long

'METHODS

Public Function FileSystem_Close(ByVal lHandle As Long) As Boolean

    FileSystem_Close = CloseHandle(lHandle)

End Function

Public Function FileSystem_Delete(ByRef sPath As String) As Boolean

    FileSystem_Delete = DeleteFileW(StrPtr(sPath))

End Function

Public Function FileSystem_FindFirstFile(ByRef sPath As String, ByRef uOutFindData As TFINDDATA) As Long

    uOutFindData.lReserved1 = VarPtr(uOutFindData)

    FileSystem_FindFirstFile = FindFirstFileW(StrPtr(sPath), uOutFindData.lReserved1)

End Function

Public Function FileSystem_FindNextFile(ByVal lHandle As Long, ByRef uOutFindData As TFINDDATA) As Boolean

    Dim l_Pointer As Long

    If uOutFindData.lReserved1 Then l_Pointer = uOutFindData.lReserved1 Else l_Pointer = VarPtr(uOutFindData)

    FileSystem_FindNextFile = FindNextFileW(lHandle, l_Pointer)

End Function

Public Function FileSystem_GetAttributes(ByRef sPath As String) As ENUM_FILESYSTEM_ATTRIBUTES

    FileSystem_GetAttributes = GetFileAttributesW(StrPtr(sPath))

End Function

Public Function FileSystem_GetSize(ByVal lHandle As Long) As Double

    Dim l_SizeHigh As Long
    Dim l_SizeLow As Long

    l_SizeLow = GetFileSize(lHandle, l_SizeHigh)

    If l_SizeLow <> -1& Then FileSystem_GetSize = ((SIZE_OFFSET * -2#) * (l_SizeHigh + ((l_SizeLow < 0&) * -1&))) + l_SizeLow Else FileSystem_GetSize = l_SizeLow

End Function

Public Function FileSystem_Open(ByRef sPath As String, ByVal lAccess As ENUM_FILESYSTEM_ACCESS, ByVal lShare As ENUM_FILESYSTEM_SHARE, ByVal lCreation As ENUM_FILESYSTEM_CREATION, Optional ByVal lAttributes As ENUM_FILESYSTEM_ATTRIBUTES, Optional ByVal lFlags As ENUM_FILESYSTEM_FLAGS) As Long

    FileSystem_Open = CreateFileW(StrPtr(sPath), lAccess, lShare, 0&, lCreation, lAttributes Or lFlags, 0&)

End Function

Public Function FileSystem_Read(ByVal lHandle As Long, ByVal lBuffer As Long, ByVal lBytesToRead As Long, ByRef lBytesRead As Long) As Boolean

    FileSystem_Read = ReadFile(lHandle, ByVal lBuffer, lBytesToRead, lBytesRead, ByVal 0&)

End Function

Public Function FileSystem_ReadAsync(ByVal lHandle As Long, ByVal lOutBuffer As Long, ByVal lBytesToRead As Long, ByVal lOffsetLow As Long, ByVal lOffsetHigh As Long, Optional ByVal lEvent As Long) As Boolean

    Dim l_Overlapped As TOVERLAPPED

    With l_Overlapped
        .lOffset = lOffsetLow
        .lOffsetHigh = lOffsetHigh
        .lhEvent = lEvent
    End With

    FileSystem_ReadAsync = ReadFile(lHandle, ByVal lOutBuffer, lBytesToRead, 0&, l_Overlapped)

End Function

Public Function FileSystem_SetAttributes(ByRef sPath As String, ByVal lAttributes As ENUM_FILESYSTEM_ATTRIBUTES) As Boolean

    FileSystem_SetAttributes = SetFileAttributesW(StrPtr(sPath), lAttributes)

End Function

Public Function FileSystem_Write(ByVal lHandle As Long, ByVal lBuffer As Long, ByVal lBytesToWrite As Long, ByRef lBytesWritten As Long) As Boolean

    FileSystem_Write = WriteFile(lHandle, ByVal lBuffer, lBytesToWrite, lBytesWritten, ByVal 0&)

End Function

Public Function FileSystem_WriteAsync(ByVal lHandle As Long, ByVal lBytes As Long, ByVal lBytesCount As Long, ByVal lOffsetLow As Long, ByVal lOffsetHigh As Long, Optional ByVal lEvent As Long) As Boolean

    Dim l_Overlapped As TOVERLAPPED

    With l_Overlapped
        .lOffset = lOffsetLow
        .lOffsetHigh = lOffsetHigh
        .lhEvent = lEvent
    End With

    FileSystem_WriteAsync = WriteFile(lHandle, ByVal lBytes, lBytesCount, 0&, l_Overlapped)

End Function
