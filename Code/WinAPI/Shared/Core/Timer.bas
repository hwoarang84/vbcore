Attribute VB_Name = "Z____WinAPI_Shared_Core_Timer"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Private Declare Function QueryPerformanceFrequency Lib "kernel32" (ByRef cFrequency As Currency) As Long
Private Declare Function QueryPerformanceCounter Lib "kernel32" (ByRef cCounter As Currency) As Long

'METHODS

Public Function Timer_QueryCounter(ByRef cOutCounter As Currency) As Boolean

    Timer_QueryCounter = QueryPerformanceCounter(cOutCounter)

End Function

Public Function Timer_QueryFrequency(ByRef cOutFrequency As Currency, ByRef cOutOverhead As Currency) As Boolean

    Dim l_Counter1 As Currency
    Dim l_Counter2 As Currency

    If QueryPerformanceFrequency(cOutFrequency) Then

        QueryPerformanceCounter l_Counter1
        QueryPerformanceCounter l_Counter2

        cOutOverhead = l_Counter1 - l_Counter2

        Timer_QueryFrequency = True

    End If

End Function
