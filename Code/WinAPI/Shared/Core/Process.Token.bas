Attribute VB_Name = "Z____WinAPI_Shared_Core_Process_Token"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_PROCESS_TOKEN_ACCESS
    TOKEN_ADJUST_DEFAULT& = 128&
    TOKEN_ADJUST_GROUPS& = 64&
    TOKEN_ADJUST_PRIVILEGES& = 32&
    TOKEN_ADJUST_SESSIONID& = 256&
    TOKEN_ASSIGN_PRIMARY& = 1&
    TOKEN_DUPLICATE& = 2&
    TOKEN_EXECUTE& = &H20000
    TOKEN_IMPERSONATE& = 4&
    TOKEN_QUERY& = 8&
    TOKEN_QUERY_SOURCE& = 16&
    TOKEN_READ& = TOKEN_QUERY Or TOKEN_EXECUTE
    TOKEN_WRITE& = TOKEN_EXECUTE Or TOKEN_ADJUST_DEFAULT Or TOKEN_ADJUST_GROUPS Or TOKEN_ADJUST_PRIVILEGES
End Enum

'TYPES

Private Type TTOKEN
    lLow As Long
    lHigh As Long
End Type

Private Type TTOKEN_PRIVILEGES
    lPrivilegeCount As Long
    uLuid As TTOKEN
    lAttributes As Long
End Type

'CONSTANTS

Private Const ATTRIBUTES_COUNT As Long = 2&
Private Const PRIVILEGE_COUNT As Long = 1&

'DECLARATIONS

Private Declare Function AdjustTokenPrivileges Lib "advapi32" (ByVal lHandleToken As Long, ByVal lDisablePrivileges As Long, uNewState As TTOKEN_PRIVILEGES, ByVal lBufferLen As Long, ByVal lPrevState As Long, ByVal lReturnLen As Long) As Long
Private Declare Function LookupPrivilegeValueW Lib "advapi32" (ByVal lSystemName As Long, ByVal lName As Long, ByRef uToken As TTOKEN) As Long
Private Declare Function OpenProcessToken Lib "advapi32" (ByVal lProcessHandle As Long, ByVal lDesiredAccess As Long, ByRef lTokenHandle As Long) As Long

'METHODS

Public Function Process_TokenAdjust(ByVal lHandle As Long, ByRef sPrivilege As String) As Boolean

    Dim l_Luid As TTOKEN
    Dim l_Privilege As TTOKEN_PRIVILEGES

    If LookupPrivilegeValueW(0&, StrPtr(sPrivilege), l_Luid) Then

        With l_Privilege
            .lAttributes = ATTRIBUTES_COUNT
            .uLuid = l_Luid
            .lPrivilegeCount = PRIVILEGE_COUNT
        End With

        Process_TokenAdjust = AdjustTokenPrivileges(lHandle, 0&, l_Privilege, 0&, 0&, 0&)

    End If

End Function

Public Function Process_TokenOpen(ByVal lHandle As Long, ByVal lFlags As ENUM_PROCESS_TOKEN_ACCESS, ByRef lOutToken As Long) As Boolean

    Process_TokenOpen = OpenProcessToken(lHandle, lFlags, lOutToken)

End Function
