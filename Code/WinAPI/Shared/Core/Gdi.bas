Attribute VB_Name = "Z____WinAPI_Shared_Core_Gdi"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_GDI_DEVICECAPS
    ASPECTX& = 40&
    ASPECTXY& = 44&
    ASPECTY& = 42&
    BITSPIXEL& = 12&
    BLTALIGNMENT& = 119&
    CLIPCAPS& = 36&
    COLORMGMTCAPS& = 121&
    COLORRES& = 108&
    CURVECAPS& = 28&
    DRIVERVERSION& = 0&
    HORZRES& = 8&
    HORZSIZE& = 4&
    LINECAPS& = 30&
    LOGPIXELSX& = 88&
    LOGPIXELSY& = 90&
    NUMBRUSHES& = 16&
    NUMCOLORS& = 24&
    NUMFONTS& = 22&
    NUMPENS& = 18&
    NUMRESERVED& = 106&
    PDEVICESIZE& = 26&
    PHYSICALHEIGHT& = 111&
    PHYSICALOFFSETX& = 112&
    PHYSICALOFFSETY& = 113&
    PHYSICALWIDTH& = 110&
    PLANES& = 14&
    POLYGONALCAPS& = 32&
    RASTERCAPS& = 38&
    SCALINGFACTORX& = 114&
    SCALINGFACTORY& = 115&
    SHADEBLENDCAPS& = 120&
    SIZEPALETTE& = 104&
    TECHNOLOGY& = 2&
    TEXTCAPS& = 34&
    VERTRES& = 10&
    VERTSIZE& = 6&
    VREFRESH& = 116&
End Enum

'CONSTANTS

Private Const DIV_DENOMINATOR As Long = 72&

'DECLARATIONS

Private Declare Function GetDeviceCaps Lib "gdi32" (ByVal lHdc As Long, ByVal lIndex As Long) As Long
Private Declare Function MulDiv Lib "kernel32" (ByVal lNumber As Long, ByVal lNumerator As Long, ByVal lDenominator As Long) As Long

'METHODS

Public Function Gdi_GetDeviceCaps(ByVal lHdc As Long, ByVal lCaps As ENUM_GDI_DEVICECAPS) As Long

    Gdi_GetDeviceCaps = GetDeviceCaps(lHdc, lCaps)

End Function

Public Function Gdi_MulDiv(ByVal lHdc As Long, ByVal lSize As Long) As Long

    Gdi_MulDiv = -MulDiv(lSize, GetDeviceCaps(lHdc, LOGPIXELSY), DIV_DENOMINATOR)

End Function
