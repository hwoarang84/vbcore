Attribute VB_Name = "Z____WinAPI_Shared_Core_String_Test"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_STRING_UNICODETEST
    IS_TEXT_UNICODE_ASCII16& = 1&
    IS_TEXT_UNICODE_CONTROLS& = 4&
    IS_TEXT_UNICODE_DBCS_LEADBYTE& = 1024&
    IS_TEXT_UNICODE_ILLEGAL_CHARS& = 256&
    IS_TEXT_UNICODE_NOT_ASCII_MASK& = 61440
    IS_TEXT_UNICODE_NOT_UNICODE_MASK& = 3840&
    IS_TEXT_UNICODE_NULL_BYTES& = 4096&
    IS_TEXT_UNICODE_ODD_LENGTH& = 512&
    IS_TEXT_UNICODE_REVERSE_ASCII16& = 16&
    IS_TEXT_UNICODE_REVERSE_CONTROLS& = 64&
    IS_TEXT_UNICODE_REVERSE_MASK& = 240&
    IS_TEXT_UNICODE_REVERSE_SIGNATURE& = 128&
    IS_TEXT_UNICODE_REVERSE_STATISTICS& = 32&
    IS_TEXT_UNICODE_SIGNATURE& = 8&
    IS_TEXT_UNICODE_STATISTICS& = 2&
    IS_TEXT_UNICODE_UNICODE_MASK& = 15&
End Enum

'DECLARATIONS

Private Declare Function IsTextUnicode Lib "advapi32" (ByVal lBuffer As Long, ByVal lBufferLen As Long, ByRef lResult As Long) As Long

'METHODS

Public Function String_TestUnicode(ByVal lBytes As Long, ByVal lBytesCount As Long, ByVal lTest As ENUM_STRING_UNICODETEST) As Boolean

    String_TestUnicode = IsTextUnicode(lBytes, lBytesCount, lTest)

End Function
