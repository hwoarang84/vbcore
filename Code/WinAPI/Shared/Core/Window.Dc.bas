Attribute VB_Name = "Z____WinAPI_Shared_Core_Window_Dc"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Public Declare Function Window_GetDC Lib "user32" Alias "GetDC" (ByVal lhWnd As Long) As Long

Private Declare Function ReleaseDC Lib "user32" (ByVal lhWnd As Long, ByVal lHdc As Long) As Long

'METHODS

Public Function Window_ReleaseDC(ByVal lhWnd As Long, ByVal lHdc As Long) As Boolean

    Window_ReleaseDC = ReleaseDC(lhWnd, lHdc)

End Function
