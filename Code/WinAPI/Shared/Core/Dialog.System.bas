Attribute VB_Name = "Z____WinAPI_Shared_Core_Dialog_System"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Enums\System.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_SYSTEM_RUNBOX
    RFD_NOBROWSE& = 1&
    RFD_NODEFFILE& = 2&
    RFD_NOSEPMEMORY_BOX& = 32&
    RFD_NOSHOWOPEN& = 8&
    RFD_USEFULLPATHDIR& = 4&
    RFD_WOW_APP& = 16&
End Enum

'CONSTANTS

Private Const CHAR_SEPARATOR As String = "#"

'DECLARATIONS

Private Declare Function ShellAboutW Lib "shell32" (ByVal lhWnd As Long, ByVal lAppName As Long, ByVal lOther As Long, ByVal lIcon As Long) As Long
Private Declare Function SHRestartSystem Lib "shell32" Alias "#59" (ByVal lhWnd As Long, ByVal lPrompt As Long, ByVal lFlags As Long) As Long
Private Declare Function SHRunDialog Lib "shell32" Alias "#61" (ByVal lhWnd As Long, ByVal lIcon As Long, ByVal lDir As Long, ByVal lTitle As Long, ByVal lPrompt As Long, ByVal lFlags As Long) As Long

'METHODS

Public Function System_AboutBox(ByRef sApplication As String, Optional ByRef sTitle As String, Optional ByRef sInformation As String, Optional ByVal lIcon As Long, Optional ByVal lhWnd As Long) As Boolean

    System_AboutBox = ShellAboutW(lhWnd, StrPtr(sTitle & CHAR_SEPARATOR & sApplication & vbNullChar), StrPtr(sInformation & vbNullChar), lIcon)

End Function

Public Function System_ExitBox(Optional ByRef sPrompt As String, Optional ByVal lFlags As ENUM_SYSTEM_EXIT, Optional ByVal lhWnd As Long) As Long

    System_ExitBox = SHRestartSystem(lhWnd, StrPtr(sPrompt & vbNullChar), lFlags)

End Function

Public Function System_RunBox(Optional ByRef sPrompt As String, Optional ByRef sTitle As String, Optional ByVal lFlags As ENUM_SYSTEM_RUNBOX, Optional ByVal lIcon As Long, Optional ByVal lhWnd As Long) As Long

    System_RunBox = SHRunDialog(lhWnd, lIcon, 0&, StrPtr(sTitle), StrPtr(sPrompt), lFlags)

End Function
