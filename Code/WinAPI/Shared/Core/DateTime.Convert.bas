Attribute VB_Name = "Z____WinAPI_Shared_Core_DateTime_Convert"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Types\DateTime.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Public Declare Function DateTime_ConvertFileToLocal Lib "kernel32" Alias "FileTimeToLocalFileTime" (ByRef uFileTime As Any, ByRef uOutLocalTime As Any) As Boolean
Public Declare Function DateTime_ConvertFileToSystem Lib "kernel32" Alias "FileTimeToSystemTime" (ByRef uFileTime As Any, ByRef uOutSystemTime As TSYSTEMTIME) As Boolean
Public Declare Function DateTime_ConvertSystemToFile Lib "kernel32" Alias "SystemTimeToFileTime" (ByRef uSystemTime As TSYSTEMTIME, ByRef uOutFileTime As Any) As Boolean
