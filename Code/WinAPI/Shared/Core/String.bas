Attribute VB_Name = "Z____WinAPI_Shared_Core_String"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'DECLARATIONS

Public Declare Function String_GetCharsCount Lib "kernel32" Alias "lstrlenW" (ByVal sString As Long) As Long

'METHODS

Public Function String_GetBytesCount(ByVal sString As Long) As Long

    String_GetBytesCount = String_GetCharsCount(sString) * 2&

End Function

Public Function String_GetLengthBeforeNullChar(ByRef sStr As String) As Long

    Dim l_Position As Long

    l_Position = InStr(1&, sStr, vbNullChar, vbBinaryCompare)

    If l_Position Then String_GetLengthBeforeNullChar = l_Position - 1& Else String_GetLengthBeforeNullChar = Len(sStr)

End Function
