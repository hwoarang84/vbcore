Attribute VB_Name = "Z_____WinAPI_Shared_Core_Enums_Gdi"
Option Explicit

'ENUMERATIONS

Public Enum ENUM_GDI_FONTQUALITY
    ANTIALIASED_QUALITY& = 4&
    CLEARTYPE_QUALITY& = 5&
    DEFAULT_QUALITY& = 0&
    DRAFT_QUALITY& = 1&
    NONANTIALIASED_QUALITY& = 3&
    PROOF_QUALITY& = 2&
End Enum
