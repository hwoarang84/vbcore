Attribute VB_Name = "Z_____WinAPI_Shared_Core_Enums_System"
Option Explicit

'ENUMERATIONS

Public Enum ENUM_SYSTEM_EXIT
    EWX_HYBRID_SHUTDOWN& = 4194304
    EWX_FORCE& = 4&
    EWX_FORCEIFHUNG& = 16&
    EWX_LOGOFF& = 0&
    EWX_POWEROFF& = 8&
    EWX_REBOOT& = 2&
    EWX_RESTARTAPPS& = 64&
    EWX_SHUTDOWN& = 1&
End Enum
