Attribute VB_Name = "Z_____WinAPI_Shared_Core_Enums_Window"
Option Explicit

'ENUMERATIONS

Public Enum ENUM_WINDOW_SHOW
    SW_FORCEMINIMIZE& = 11&
    SW_HIDE& = 0&
    SW_MAXIMIZE& = 3&
    SW_MINIMIZE& = 6&
    SW_RESTORE& = 9&
    SW_SHOW& = 5&
    SW_SHOWDEFAULT& = 10&
    SW_SHOWMAXIMIZED& = 3&
    SW_SHOWMINIMIZED& = 2&
    SW_SHOWMINNOACTIVE& = 7&
    SW_SHOWNA& = 8&
    SW_SHOWNOACTIVE& = 4&
    SW_SHOWNORMAL& = 1&
End Enum
