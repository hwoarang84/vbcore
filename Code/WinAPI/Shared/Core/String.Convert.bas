Attribute VB_Name = "Z____WinAPI_Shared_Core_String_Convert"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_CHAR_CONVERT
    MB_COMPOSITE& = 2&
    MB_ERR_INVALID_CHARS& = 8&
    MB_PRECOMPOSED& = 1&
    MB_USEGLYPHCHARS& = 4&
End Enum

'CONSTANTS

Private Const CODE_PAGE_UTF8 As Long = 65001

'DECLARATIONS

Public Declare Function String_ConvertToLower Lib "user32" Alias "CharLowerBuffW" (ByVal sString As Long, ByVal lStringLen As Long) As Long
Public Declare Function String_ConvertToUpper Lib "user32" Alias "CharUpperBuffW" (ByVal sString As Long, ByVal lStringLen As Long) As Long

Private Declare Function MultiByteToWideChar Lib "kernel32" (ByVal lCodePage As Long, ByVal lFlags As Long, ByVal lMultiByte As Long, ByVal lMultiByteLen As Long, ByVal lWideCharStr As Long, ByVal lWideCharStrLen As Long) As Long
Private Declare Function WideCharToMultiByte Lib "kernel32" (ByVal lCodePage As Long, ByVal lFlags As Long, ByVal lWideCharStr As Long, ByVal lWideCharStrLen As Long, ByVal bMultiByte As Long, ByVal lMultiByteLen As Long, ByVal lDefaultChar As Long, ByVal lUsedDefaultChar As Long) As Long

'METHODS

Public Function String_ConvertFromUTF8(ByVal lFlags As ENUM_CHAR_CONVERT, ByVal lBytes As Long, ByVal lBytesLen As Long, ByVal sString As Long, ByVal lStringLen As Long) As Long

    String_ConvertFromUTF8 = MultiByteToWideChar(CODE_PAGE_UTF8, lFlags, lBytes, lBytesLen, sString, lStringLen)

End Function

Public Function String_ConvertToUTF8(ByVal sString As Long, ByVal lStringLen As Long, ByVal lBytes As Long, ByVal lBytesLen As Long) As Long

    String_ConvertToUTF8 = WideCharToMultiByte(CODE_PAGE_UTF8, 0&, sString, lStringLen, lBytes, lBytesLen, 0&, 0&)

End Function
