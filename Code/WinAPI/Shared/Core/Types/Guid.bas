Attribute VB_Name = "Z_____WinAPI_Shared_Core_Types_Guid"
Option Explicit

'TYPES

Public Type TGUID
    lData1 As Long
    iData2 As Integer
    iData3 As Integer
    bData4(7) As Byte
End Type

