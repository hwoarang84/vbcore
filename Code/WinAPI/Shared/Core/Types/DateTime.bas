Attribute VB_Name = "Z_____WinAPI_Shared_Core_Types_DateTime"
Option Explicit

'TYPES

Public Type TFILETIME
    lLowDateTime As Long
    lHighDateTime As Long
End Type

Public Type TSYSTEMTIME
    iYear As Integer
    iMonth As Integer
    iDayOfWeek As Integer
    iDay As Integer
    iHour As Integer
    iMinute As Integer
    iSecond As Integer
    iMilliseconds As Integer
End Type
