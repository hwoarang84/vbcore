Attribute VB_Name = "Z____WinAPI_Shared_Core_Memory"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Private Const CONST_BYTES_4 As Long = 4&

'DECLARATIONS

Public Declare Sub Memory_Clear Lib "kernel32" Alias "RtlZeroMemory" (ByRef uAddress As Any, ByVal lLength As Long)
Public Declare Sub Memory_Copy Lib "kernel32" Alias "RtlMoveMemory" (ByRef uAddressTarget As Any, ByRef uAddressSource As Any, ByVal lLength As Long)
Public Declare Sub Memory_CopyFast Lib "msvbvm60" Alias "__vbaCopyBytes" (ByVal lLength As Long, ByRef uAddressTarget As Any, ByRef uAddressSource As Any)
Public Declare Sub Memory_CopyZero Lib "msvbvm60" Alias "__vbaCopyBytesZero" (ByVal lLength As Long, ByRef uAddressTarget As Any, ByRef uAddressSource As Any)
Public Declare Sub Memory_Fill Lib "kernel32" Alias "RtlFillMemory" (ByRef uAddress As Any, ByVal lLength As Long, ByVal bValue As Byte)
Public Declare Sub Memory_Get2 Lib "msvbvm60" Alias "GetMem2" (ByRef uAddress As Any, ByRef uRetValue As Any)
Public Declare Sub Memory_Get4 Lib "msvbvm60" Alias "GetMem4" (ByRef uAddress As Any, ByRef uRetValue As Any)
Public Declare Sub Memory_Get8 Lib "msvbvm60" Alias "GetMem8" (ByRef uAddress As Any, ByRef uRetValue As Any)
Public Declare Sub Memory_Put2 Lib "msvbvm60" Alias "PutMem2" (ByRef uAddress As Any, ByRef uRetValue As Any)
Public Declare Sub Memory_Put4 Lib "msvbvm60" Alias "PutMem4" (ByRef uAddress As Any, ByRef uRetValue As Any)
Public Declare Sub Memory_Put8 Lib "msvbvm60" Alias "PutMem8" (ByRef uAddress As Any, ByRef uRetValue As Any)

'METHODS

Public Function Memory_CopyByRef4(ByRef lPointer As Long) As Long

    Memory_Copy Memory_CopyByRef4, lPointer, CONST_BYTES_4

End Function

Public Function Memory_CopyByVal4(ByVal lPointer As Long) As Long

    Memory_Copy Memory_CopyByVal4, ByVal lPointer, CONST_BYTES_4

End Function
