Attribute VB_Name = "Z____WinAPI_Shared_Core_Window"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Enums\Window.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_WINDOW_OFFSET
    GWL_EXSTYLE& = -20&
    GWL_HINSTANCE& = -6&
    GWL_ID& = -12&
    GWL_STYLE& = -16&
    GWL_USERDATA& = -21&
    GWL_WNDPROC& = -4&
End Enum

Public Enum ENUM_WINDOW_REDRAW
    RDW_ERASE& = 4&
    RDW_ERASENOW& = 512&
    RDW_FRAME& = 1024&
    RDW_INTERNALPAINT& = 2&
    RDW_INVALIDATE& = 1&
    RDW_NOCHILDREN& = 64&
    RDW_NOERASE& = 32&
    RDW_NOFRAME& = 2048&
    RDW_NOINTERNALPAINT& = 16&
    RDW_UPDATENOW& = 256&
    RDW_VALIDATE& = 8&
End Enum

Public Enum ENUM_WINDOW_RESIZE
    SWP_ASYNCWINDOWPOS& = 16384&
    SWP_DEFERERASE& = 8192&
    SWP_DRAWFRAME& = 32&
    SWP_FRAMECHANGED& = SWP_DRAWFRAME&
    SWP_HIDEWINDOW& = 128&
    SWP_NOACTIVATE& = 16&
    SWP_NOCOPYBITS& = 256&
    SWP_NOMOVE& = 2&
    SWP_NOOWNERZORDER& = 512&
    SWP_NOREDRAW& = 8&
    SWP_NOREPOSITION& = SWP_NOOWNERZORDER&
    SWP_NOSENDCHANGING& = 1024&
    SWP_NOSIZE& = 1&
    SWP_NOZORDER& = 4&
    SWP_SHOWWINDOW& = 64&
End Enum

Public Enum ENUM_WINDOW_RELATIONSHIP
    GW_CHILD& = 5&
    GW_ENABLEDPOPUP& = 6&
    GW_HWNDFIRST& = 0&
    GW_HWNDLAST& = 1&
    GW_HWNDNEXT& = 2&
    GW_HWNDPREV& = 3&
    GW_OWNER& = 4&
End Enum

Public Enum ENUM_WINDOW_STYLE
    WS_BORDER& = 8388608
    WS_CAPTION& = 12582912
    WS_CHILD& = 1073741824
    WS_CHILDWINDOW& = WS_CHILD
    WS_CLIPCHILDREN& = 33554432
    WS_CLIPSIBLINGS& = 67108864
    WS_DISABLED& = 134217728
    WS_DLGFRAME& = 4194304
    WS_GROUP& = 131072
    WS_HSCROLL& = 1048576
    WS_ICONIC& = 536870912
    WS_MAXIMIZE& = 16777216
    WS_MAXIMIZEBOX& = 65536
    WS_MINIMIZE& = WS_ICONIC
    WS_MINIMIZEBOX& = 131072
    WS_OVERLAPPED& = 0&
    WS_POPUP& = &H80000000
    WS_SIZEBOX& = 262144
    WS_SYSMENU& = 524288
    WS_TABSTOP& = WS_MAXIMIZEBOX
    WS_THICKFRAME& = WS_SIZEBOX
    WS_TILED& = WS_OVERLAPPED
    WS_VISIBLE& = 268435456
    WS_VSCROLL& = 2097152
    WS_OVERLAPPEDWINDOW& = WS_OVERLAPPED Or WS_CAPTION Or WS_SYSMENU Or WS_THICKFRAME Or WS_MINIMIZEBOX Or WS_MAXIMIZEBOX
    WS_POPUPWINDOW& = WS_POPUP Or WS_BORDER Or WS_SYSMENU
    WS_TILEDWINDOW& = WS_OVERLAPPEDWINDOW
End Enum

Public Enum ENUM_WINDOW_STYLEEX
    WS_EX_ACCEPTFILES& = 16&
    WS_EX_APPWINDOW& = 262144
    WS_EX_CLIENTEDGE& = 512&
    WS_EX_COMPOSITED& = 33554432
    WS_EX_CONTEXTHELP& = 1024&
    WS_EX_CONTROLPARENT& = 65536
    WS_EX_DLGMODALFRAME& = 1&
    WS_EX_LAYERED& = 524288
    WS_EX_LAYOUTRTL& = 4194304
    WS_EX_LEFT& = 0&
    WS_EX_LEFTSCROLLBAR& = 16384&
    WS_EX_LTRREADING& = 0&
    WS_EX_MDICHILD& = 64&
    WS_EX_NOACTIVATE& = 134217728
    WS_EX_NOINHERITLAYOUT& = 1048576
    WS_EX_NOPARENTNOTIFY& = 4&
    WS_EX_NOREDIRECTIONBITMAP& = 2097152
    WS_EX_RIGHT& = 4096&
    WS_EX_RIGHTSCROLLBAR& = 0&
    WS_EX_RTLREADING& = 8192&
    WS_EX_STATICEDGE& = 131072
    WS_EX_TOOLWINDOW& = 128&
    WS_EX_TOPMOST& = 8&
    WS_EX_TRANSPARENT& = 32&
    WS_EX_WINDOWEDGE& = 256&
    WS_EX_OVERLAPPEDWINDOW& = WS_EX_WINDOWEDGE& Or WS_EX_CLIENTEDGE&
    WS_EX_PALETTEWINDOW& = WS_EX_WINDOWEDGE& Or WS_EX_TOOLWINDOW& Or WS_EX_TOPMOST&
End Enum

'CONSTANTS

Private Const MAX_CAPTION_LENGTH As Long = 512&

'DECLARATIONS

Public Declare Function Window_FindActive Lib "user32" Alias "GetActiveWindow" () As Long
Public Declare Function Window_FindFocused Lib "user32" Alias "GetFocus" () As Long
Public Declare Function Window_FindForeground Lib "user32" Alias "GetForegroundWindow" () As Long
Public Declare Function Window_FindParent Lib "user32" Alias "GetParent" (ByVal lhWnd As Long) As Long
Public Declare Function Window_Focus Lib "user32" Alias "SetFocus" (ByVal lhWnd As Long) As Long
Public Declare Function Window_GetDesktop Lib "user32" Alias "GetDesktopWindow" () As Long

Private Declare Function CreateWindowExW Lib "user32" (ByVal lExStyle As Long, ByVal lClassName As Long, ByVal lWindowName As Long, ByVal lStyle As Long, ByVal lX As Long, ByVal lY As Long, ByVal lWidth As Long, ByVal lHeight As Long, ByVal lhWndParent As Long, ByVal lMenu As Long, ByVal lInstance As Long, ByVal lParam As Long) As Long
Private Declare Function DestroyWindow Lib "user32" (ByVal lhWnd As Long) As Long
Private Declare Function EnableWindow Lib "user32" (ByVal lhWnd As Long, ByVal lEnable As Long) As Long
Private Declare Function FindWindowW Lib "user32" (ByVal lClass As Long, ByVal lWindow As Long) As Long
Private Declare Function GetWindow Lib "user32" (ByVal lhWnd As Long, ByVal lCmd As Long) As Long
Private Declare Function GetWindowTextW Lib "user32" (ByVal lhWnd As Long, ByVal lString As Long, ByVal lLen As Long) As Long
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal lhWnd As Long, ByRef lProcessId As Long) As Long
Private Declare Function IsWindowVisible Lib "user32" (ByVal lhWnd As Long) As Long
Private Declare Function RedrawWindow Lib "user32" (ByVal lhWnd As Long, ByRef uRect As Any, ByVal lUpdate As Long, ByVal lFlags As Long) As Long
Private Declare Function SetWindowLongW Lib "user32" (ByVal lhWnd As Long, ByVal lIndex As Long, ByVal lNewLong As Long) As Long
Private Declare Function SetWindowPos Lib "user32" (ByVal lhWnd As Long, ByVal lhWndInsertAfter As Long, ByVal lX As Long, ByVal lY As Long, ByVal lWidth As Long, ByVal lHeight As Long, ByVal lFlags As Long) As Long
Private Declare Function ShowWindow Lib "user32" (ByVal lhWnd As Long, ByVal lFlags As Long) As Long

'METHODS

Public Function Window_Create(ByRef sClassName As String, ByRef sWindowName As String, ByVal lStyle As ENUM_WINDOW_STYLE, ByVal lStyleEx As ENUM_WINDOW_STYLEEX, ByVal lX As Long, ByVal lY As Long, ByVal lWidth As Long, ByVal lHeight As Long, ByVal lhWndParent As Long, ByVal lMenu As Long, ByVal lInstance As Long, Optional ByVal lParam As Long) As Long

    Window_Create = CreateWindowExW(lStyleEx, StrPtr(sClassName), StrPtr(sWindowName), lStyle, lX, lY, lWidth, lHeight, lhWndParent, lMenu, lInstance, lParam)

End Function

Public Function Window_Change(ByVal lhWnd As Long, ByVal lOffset As ENUM_WINDOW_OFFSET, ByVal lData As Long) As Long

    Window_Change = SetWindowLongW(lhWnd, lOffset, lData)

End Function

Public Function Window_Destroy(ByVal lhWnd As Long) As Boolean

    Window_Destroy = DestroyWindow(lhWnd)

End Function

Public Function Window_Enable(ByVal lhWnd As Long, ByVal bEnabled As Boolean) As Boolean

    Window_Enable = EnableWindow(lhWnd, -bEnabled)

End Function

Public Function Window_FindByClass(ByRef sClass As String) As Long

    Window_FindByClass = FindWindowW(StrPtr(sClass), 0&)

End Function

Public Function Window_FindByTitle(ByRef sTitle As String) As Long

    Window_FindByTitle = FindWindowW(0&, StrPtr(sTitle))

End Function

Public Function Window_FindRelative(ByVal lhWnd As Long, ByVal lRelationship As ENUM_WINDOW_RELATIONSHIP) As Long

    Window_FindRelative = GetWindow(lhWnd, lRelationship)

End Function

Public Function Window_GetProcessId(ByVal lhWnd As Long) As Long

    Dim l_ProcessId As Long

    GetWindowThreadProcessId lhWnd, l_ProcessId

    Window_GetProcessId = l_ProcessId

End Function

Public Function Window_GetText(ByVal lhWnd As Long, ByRef sOutText As String) As Boolean

    Dim l_Length As Long
    Dim l_Text As String

    l_Text = Space$(MAX_CAPTION_LENGTH)
    l_Length = GetWindowTextW(lhWnd, StrPtr(l_Text), MAX_CAPTION_LENGTH)

    If l_Length Then

        sOutText = Left$(l_Text, l_Length)

        Window_GetText = True

    End If

End Function

Public Function Window_GetVisible(ByVal lhWnd As Long) As Boolean

    Window_GetVisible = IsWindowVisible(lhWnd)

End Function

Public Function Window_Redraw(ByVal lhWnd As Long, ByVal lFlags As ENUM_WINDOW_REDRAW) As Boolean

    Window_Redraw = RedrawWindow(lhWnd, ByVal 0&, 0&, lFlags)

End Function

Public Function Window_Resize(ByVal lhWnd As Long, ByVal lhWndInsertAfter As Long, ByVal lX As Long, ByVal lY As Long, ByVal lWidth As Long, ByVal lHeight As Long, ByVal lFlags As ENUM_WINDOW_RESIZE) As Boolean

    Window_Resize = SetWindowPos(lhWnd, lhWndInsertAfter, lX, lY, lWidth, lHeight, lFlags)

End Function

Public Function Window_Show(ByVal lhWnd As Long, ByVal lFlags As ENUM_WINDOW_SHOW) As Boolean

    Window_Show = ShowWindow(lhWnd, lFlags)

End Function
