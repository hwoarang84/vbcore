Attribute VB_Name = "Z_____WinAPI_Shared_Core_Constants_FileSystem"
Option Explicit

'CONSTANTS

Public Const CHAR_SLASH As String = "\"
Public Const CHAR_VBAR As String = "|"

Public Const MAX_PATH As Long = 260&
Public Const MAX_PATH_EX As Long = 32767&
