Attribute VB_Name = "Z_____WinAPI_Shared_Core_Constants_Window"
Option Explicit

'CONSTANTS

Public Const WM_ACTIVATE As Long = 6&
Public Const WM_CHANGEUISTATE As Long = 295&
Public Const WM_CHAR As Long = 258&
Public Const WM_CHILDACTIVATE As Long = 34&
Public Const WM_COMMAND As Long = 273&
Public Const WM_CTLCOLOR As Long = 25&
Public Const WM_CTLCOLORBTN As Long = 309&
Public Const WM_CTLCOLOREDIT As Long = 307&
Public Const WM_CTLCOLORLISTBOX As Long = 308&
Public Const WM_CTLCOLORSTATIC As Long = 312&
Public Const WM_DESTROY As Long = 2&
Public Const WM_HSCROLL As Long = 276&
Public Const WM_KEYDOWN As Long = 256&
Public Const WM_KEYUP As Long = 257&
Public Const WM_KILLFOCUS As Long = 8&
Public Const WM_LBUTTONDBLCLK As Long = 515&
Public Const WM_LBUTTONDOWN As Long = 513&
Public Const WM_LBUTTONUP As Long = 514&
Public Const WM_MBUTTONDOWN As Long = 519&
Public Const WM_MBUTTONUP As Long = 520&
Public Const WM_MOUSEACTIVATE As Long = 33&
Public Const WM_MOUSEMOVE As Long = 512&
Public Const WM_MOUSEWHEEL As Long = 522&
Public Const WM_MOVE As Long = 3&
Public Const WM_NOTIFY As Long = 78&
Public Const WM_RBUTTONDOWN As Long = 516&
Public Const WM_RBUTTONUP As Long = 517&
Public Const WM_SETFOCUS As Long = 7&
Public Const WM_SETFONT As Long = 48&
Public Const WM_SETTEXT As Long = 12&
Public Const WM_SIZE As Long = 5&
Public Const WM_USER As Long = 1024&
Public Const WM_VSCROLL As Long = 277&
