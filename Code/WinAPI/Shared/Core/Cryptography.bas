Attribute VB_Name = "Z____WinAPI_Shared_Core_Cryptography"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'ENUMERATIONS

Public Enum ENUM_CRYPTOGRAPHY_FLAGS
    CRYPT_DEFAULT_CONTAINER_OPTIONAL& = 128&
    CRYPT_DELETEKEYSET& = 16&
    CRYPT_MACHINE_KEYSET& = 32&
    CRYPT_NEWKEYSET& = 8&
    CRYPT_SILENT& = 64&
    CRYPT_VERIFYCONTEXT& = &HF0000000
End Enum

Public Enum ENUM_CRYPTOGRAPHY_PROVIDER
    CRYPTOGRAPHY_PROVIDER_MSBASE10&
End Enum

'TYPES

Public Type TCRYPTOGRAPHYCONTEXT
    lHash As Long
    lKey As Long
    lProvider As Long
End Type

'CONSTANTS

Private Const CRYPTOGRAPHY_ALG_CLASS_DATA_ENCRYPT As Long = 24576&
Private Const CRYPTOGRAPHY_ALG_CLASS_HASH As Long = 32768
Private Const CRYPTOGRAPHY_ALG_SID_MD5 As Long = 3&
Private Const CRYPTOGRAPHY_ALG_SID_RC4 As Long = 1&
Private Const CRYPTOGRAPHY_ALG_TYPE_STREAM As Long = 2048&
Private Const CRYPTOGRAPHY_MS_BASE_CRYPTOGRAPHY_PROVIDER_10 As String = "Microsoft Base Cryptographic Provider v1.0"
Private Const CRYPTOGRAPHY_PROVIDERTYPE_RSAFULL As Long = 1&

'DECLARATIONS

Private Declare Function CryptAcquireContextW Lib "advapi32" (ByRef lProvider As Long, ByVal lContainer As Long, ByVal lProviderType As Long, ByVal lType As Long, ByVal lFlags As Long) As Long
Private Declare Function CryptCreateHash Lib "advapi32" (ByVal lProvider As Long, ByVal lAlgoritm As Long, ByVal lKey As Long, ByVal lFlags As Long, lHash As Long) As Long
Private Declare Function CryptDecrypt Lib "advapi32" (ByVal lKey As Long, ByVal lHash As Long, ByVal lFinal As Long, ByVal lFlags As Long, uData As Any, lDataLen As Long) As Long
Private Declare Function CryptDeriveKey Lib "advapi32" (ByVal lProvider As Long, ByVal lAlgoritm As Long, ByVal lData As Long, ByVal lFlags As Long, lKey As Long) As Long
Private Declare Function CryptDestroyHash Lib "advapi32" (ByVal lHash As Long) As Long
Private Declare Function CryptDestroyKey Lib "advapi32" (ByVal lKey As Long) As Long
Private Declare Function CryptEncrypt Lib "advapi32" (ByVal lKey As Long, ByVal lHash As Long, ByVal lFinal As Long, ByVal lFlags As Long, uData As Any, lDataLen As Long, ByVal lBufLen As Long) As Long
Private Declare Function CryptHashData Lib "advapi32" (ByVal lHash As Long, ByVal sData As Long, ByVal lLen As Long, ByVal lFlags As Long) As Long
Private Declare Function CryptReleaseContext Lib "advapi32" (ByVal lProvider As Long, ByVal lFlags As Long) As Long

'METHODS

Public Function Cryptography_Create(ByRef sContainer As String, ByVal lProvider As ENUM_CRYPTOGRAPHY_PROVIDER, ByVal lFlags As ENUM_CRYPTOGRAPHY_FLAGS, ByRef uOutContext As TCRYPTOGRAPHYCONTEXT) As Boolean

    Dim l_Context As TCRYPTOGRAPHYCONTEXT
    Dim l_ContainerPtr As Long
    Dim l_ProviderPtr As Long

    l_ContainerPtr = StrPtr(sContainer)
    l_ProviderPtr = StrPtr(CRYPTOGRAPHY_MS_BASE_CRYPTOGRAPHY_PROVIDER_10)

    If CryptAcquireContextW(l_Context.lProvider, l_ContainerPtr, l_ProviderPtr, CRYPTOGRAPHY_PROVIDERTYPE_RSAFULL, lFlags) = 0& Then

        If CryptAcquireContextW(l_Context.lProvider, l_ContainerPtr, l_ProviderPtr, CRYPTOGRAPHY_PROVIDERTYPE_RSAFULL, 0&) = 0& Then

            Exit Function

        End If

    End If

    If CryptCreateHash(l_Context.lProvider, CRYPTOGRAPHY_ALG_CLASS_HASH Or CRYPTOGRAPHY_ALG_SID_MD5, 0&, 0&, l_Context.lHash) Then

        If CryptHashData(l_Context.lHash, l_ContainerPtr, Len(sContainer), 0&) Then

            If CryptDeriveKey(l_Context.lProvider, CRYPTOGRAPHY_ALG_CLASS_DATA_ENCRYPT Or CRYPTOGRAPHY_ALG_SID_RC4 Or CRYPTOGRAPHY_ALG_TYPE_STREAM, l_Context.lHash, 0&, l_Context.lKey) Then

                uOutContext = l_Context

                Cryptography_Create = True

            End If

        End If

    End If

End Function

Public Function Cryptography_Decrypt(ByVal lKey As Long, ByVal lData As Long, ByVal lBufferLen As Long) As Boolean

    If lKey Then Cryptography_Decrypt = CryptDecrypt(lKey, 0&, 1&, 0&, ByVal lData, lBufferLen)

End Function

Public Function Cryptography_Destroy(ByRef uContext As TCRYPTOGRAPHYCONTEXT) As Boolean

    If uContext.lProvider Then

        If uContext.lKey Then CryptDestroyKey uContext.lKey

        If uContext.lHash Then CryptDestroyHash uContext.lHash

        Cryptography_Destroy = CryptReleaseContext(uContext.lProvider, 0&)

    End If

End Function

Public Function Cryptography_Encrypt(ByVal lKey As Long, ByVal lData As Long, ByVal lDataLen As Long, ByVal lBufferLen As Long) As Boolean

    If lKey Then Cryptography_Encrypt = CryptEncrypt(lKey, 0&, 1&, 0&, ByVal lData, lDataLen, lBufferLen)

End Function
