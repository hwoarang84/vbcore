Attribute VB_Name = "Z___WinAPI_Shared_Exception"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Core\Process.Library.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Private Const FORMAT_MESSAGE_IGNORE_INSERTS As Long = 512&
Private Const FORMAT_MESSAGE_FROM_HMODULE As Long = 2048&
Private Const FORMAT_MESSAGE_FROM_SYSTEM As Long = 4096&
Private Const LIB_WINHTTP As String = "winhttp"
Private Const LIB_WININET As String = "wininet"

'DECLARATIONS

Public Declare Function Exception_GetLastSystemErrorId Lib "kernel32" Alias "GetLastError" () As Long

Private Declare Function FormatMessageW Lib "kernel32" (ByVal lFlags As Long, ByVal lSource As Long, ByVal lMessageId As Long, ByVal lLanguageId As Long, ByVal lBuffer As Long, ByVal lSize As Long, ByVal lArgs As Long) As Long

'METHODS

Public Function Exception_GetSystemMessageById(ByVal lErrorId As Long, ByRef sOutMessage As String) As Boolean

    Dim l_LibHandle As Long
    Dim l_Length As Long
    Dim l_Text As String

    l_LibHandle = GetLibraryHandle(lErrorId)
    l_Text = Space$(FORMAT_MESSAGE_IGNORE_INSERTS)
    l_Length = FormatMessageW(FORMAT_MESSAGE_IGNORE_INSERTS Or FORMAT_MESSAGE_FROM_HMODULE Or FORMAT_MESSAGE_FROM_SYSTEM, l_LibHandle, lErrorId, 0&, StrPtr(l_Text), FORMAT_MESSAGE_IGNORE_INSERTS, 0&)

    If l_LibHandle Then Process_LibraryRelease l_LibHandle

    If l_Length Then

        sOutMessage = Left$(l_Text, l_Length - 2&)

        Exception_GetSystemMessageById = True

    End If

End Function

Private Function GetLibraryHandle(ByVal lErrorId As Long) As Long

    Select Case lErrorId

        Case 0& To 11999&, 12185& To 15999&: GetLibraryHandle = 0&

        Case 12000& To 12050&, 12052& To 12057&, 12110& To 12112&, 12130& To 12138&, 12150& To 12175&: GetLibraryHandle = Process_LibraryLoad(LIB_WININET)

        Case 12100& To 12103&, 12176& To 12184&: GetLibraryHandle = Process_LibraryLoad(LIB_WINHTTP)

    End Select

End Function
