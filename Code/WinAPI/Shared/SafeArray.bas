Attribute VB_Name = "Z___WinAPI_Shared_SafeArray"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  Core\Enums\Ole.bas
'  Core\Memory.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'TYPES

Public Type TSAFEARRAYBOUND
    lElements As Long
    lLowest As Long
End Type

Public Type TSAFEARRAY
    iDims As Integer
    iFeatures As Integer
    lElementSize As Long
    lLocks As Long
    lData As Long
    lPointer As Long
    lVarType As Long
    uBounds() As TSAFEARRAYBOUND
End Type

'CONSTANTS

Private Const POINTER_TO_POINTER As Long = 16384&
Private Const SAFEARRAY_OFFSET As Long = 16&

'DECLARATIONS

Public Declare Function SafeArray_PointerStatic Lib "msvbvm60" Alias "VarPtr" (ByRef uArr() As Any) As Long

Private Declare Function SafeArrayCopy Lib "oleaut32" (ByVal lSource As Long, ByVal lTarget As Long) As Long
Private Declare Function SafeArrayCopyData Lib "oleaut32" (ByVal lSource As Long, ByVal lTarget As Long) As Long
Private Declare Function SafeArrayCreate Lib "oleaut32" (ByVal lType As Integer, ByVal lDims As Long, uBounds As Any) As Long
Private Declare Function SafeArrayDestroy Lib "oleaut32" (ByVal lArray As Long) As Long
Private Declare Function SafeArrayGetElement Lib "oleaut32" (ByVal lArray As Long, ByRef lIndices As Long, uValue As Any) As Long
Private Declare Function SafeArrayPutElement Lib "oleaut32" (ByVal lArray As Long, ByRef lIndices As Long, uValue As Any) As Long
Private Declare Function SafeArrayRedim Lib "oleaut32" (ByVal lArray As Long, uLastBound As TSAFEARRAYBOUND) As Long

'METHODS

Public Function SafeArray_Copy(ByRef uSafeArray As TSAFEARRAY, ByRef uOutTarget As Variant) As ENUM_OLE_STATUS

    Memory_Copy uOutTarget, vbArray + uSafeArray.lVarType, 2&

    SafeArray_Copy = SafeArrayCopy(uSafeArray.lPointer, VarPtr(uOutTarget) + 8&)

End Function

Public Function SafeArray_CopyData(ByRef uSource As TSAFEARRAY, ByRef uTarget As TSAFEARRAY) As ENUM_OLE_STATUS

    SafeArray_CopyData = SafeArrayCopyData(uSource.lPointer, uTarget.lPointer)

End Function

Public Function SafeArray_Create(ByRef uSafeArray As TSAFEARRAY, ByVal lType As VbVarType, ByVal lDims As Long, ByRef uBounds() As TSAFEARRAYBOUND) As ENUM_OLE_STATUS

    Dim l_ArrPtr As Long

    l_ArrPtr = SafeArrayCreate(lType, lDims, uBounds(0))

    If l_ArrPtr Then SafeArray_Create = ArrayGetInfo(uSafeArray, l_ArrPtr, lType) Else SafeArray_Create = E_UNEXPECTED

End Function

Public Function SafeArray_CreateVector(ByRef uSafeArray As TSAFEARRAY, ByVal lType As VbVarType, ByVal lElements As Long) As ENUM_OLE_STATUS

    Dim l_Bounds(0) As TSAFEARRAYBOUND

    l_Bounds(0).lElements = lElements

    SafeArray_CreateVector = SafeArray_Create(uSafeArray, lType, 1&, l_Bounds)

End Function

Public Function SafeArray_Destroy(ByRef uSafeArray As TSAFEARRAY) As ENUM_OLE_STATUS

    Dim l_Result As ENUM_OLE_STATUS

    l_Result = SafeArrayDestroy(uSafeArray.lPointer)

    If l_Result = S_OK Then

        Erase uSafeArray.uBounds

        Memory_Clear uSafeArray, Len(uSafeArray)

    Else

        SafeArray_Destroy = l_Result

    End If

End Function

Public Function SafeArray_FillInfo(ByRef uSafeArray As TSAFEARRAY, ByVal lSrcArrayPtr As Long) As ENUM_OLE_STATUS

    SafeArray_FillInfo = ArrayGetInfo(uSafeArray, lSrcArrayPtr, 0&)

End Function

Public Function SafeArray_GetBoundL(ByRef uSafeArray As TSAFEARRAY, ByVal lDim As Long) As Long

    SafeArray_GetBoundL = uSafeArray.uBounds(ArrayGetDimensionIndex(uSafeArray.iDims, lDim)).lLowest

End Function

Public Function SafeArray_GetBoundU(ByRef uSafeArray As TSAFEARRAY, ByVal lDim As Long) As Long

    Dim l_DimIndex As Long

    l_DimIndex = ArrayGetDimensionIndex(uSafeArray.iDims, lDim)

    SafeArray_GetBoundU = uSafeArray.uBounds(l_DimIndex).lLowest + uSafeArray.uBounds(l_DimIndex).lElements - 1&

End Function

Public Function SafeArray_GetElement(ByRef uSafeArray As TSAFEARRAY, ByRef lIndexes As Long, ByRef uOutValue As Variant) As ENUM_OLE_STATUS

    SafeArray_GetElement = E_INVALIDARG

    Select Case uSafeArray.lVarType

        Case vbInteger To vbString, vbObject, vbError, vbBoolean, vbByte: SafeArray_GetElement = SafeArrayGetElement(uSafeArray.lPointer, lIndexes, ByVal VarPtr(uOutValue) + 8&)

        Case vbVariant: SafeArray_GetElement = SafeArrayGetElement(uSafeArray.lPointer, lIndexes, uOutValue)

        Case vbDecimal: SafeArray_GetElement = SafeArrayGetElement(uSafeArray.lPointer, lIndexes, ByVal VarPtr(uOutValue))

    End Select

End Function

Public Function SafeArray_GetElementsCount(ByRef uSafeArray As TSAFEARRAY, ByVal lDim As Long) As Long

    SafeArray_GetElementsCount = uSafeArray.uBounds(ArrayGetDimensionIndex(uSafeArray.iDims, lDim)).lElements

End Function

Public Function SafeArray_Redim(ByRef uSafeArray As TSAFEARRAY, ByVal lElements As Long) As ENUM_OLE_STATUS

    Dim l_Elements As Long
    Dim l_Result As ENUM_OLE_STATUS

    l_Elements = uSafeArray.uBounds(0&).lElements

    uSafeArray.uBounds(0&).lElements = lElements

    l_Result = SafeArrayRedim(uSafeArray.lPointer, uSafeArray.uBounds(0))

    If l_Result <> S_OK Then

        uSafeArray.uBounds(0&).lElements = l_Elements

        SafeArray_Redim = l_Result

    End If

End Function

Public Function SafeArray_SetElement(ByRef uSafeArray As TSAFEARRAY, ByRef lIndexes As Long, ByRef uValue As Variant) As ENUM_OLE_STATUS

    SafeArray_SetElement = E_INVALIDARG

    On Error GoTo ErrorHandler

    Select Case uSafeArray.lVarType

        Case vbError, vbLong: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, CLng(uValue))

        Case vbInteger: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, CInt(uValue))

        Case vbDouble: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, CDbl(uValue))

        Case vbCurrency: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, CCur(uValue))

        Case vbSingle: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, CSng(uValue))

        Case vbByte: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, CByte(uValue))

        Case vbBoolean: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, CBool(uValue))

        Case vbString: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, ByVal StrPtr(uValue))

        Case vbDate: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, CDate(uValue))

        Case vbObject: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, ByVal ObjPtr(uValue))

        Case vbVariant: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, uValue)

        Case vbDecimal: SafeArray_SetElement = SafeArrayPutElement(uSafeArray.lPointer, lIndexes, ByVal VarPtr(CDec(uValue)))

    End Select

ErrorHandler:

End Function

Private Function ArrayGetDimensionIndex(ByVal lSafeArrayDims As Long, ByVal lDim As Long) As Long

    If lDim > 0& And lDim <= lSafeArrayDims Then ArrayGetDimensionIndex = lSafeArrayDims - lDim Else ArrayGetDimensionIndex = lSafeArrayDims - 1&

End Function

Private Function ArrayGetInfo(ByRef uSafeArray As TSAFEARRAY, ByVal lSrcArrayPtr As Long, ByVal lType As Long) As ENUM_OLE_STATUS

    If lType Then

        uSafeArray.lPointer = lSrcArrayPtr
        uSafeArray.lVarType = lType

    Else

        Memory_Copy uSafeArray.lVarType, ByVal lSrcArrayPtr, 2&

        If (uSafeArray.lVarType And vbArray) = vbArray Then

            uSafeArray.lVarType = uSafeArray.lVarType Xor vbArray

        Else

            ArrayGetInfo = E_UNEXPECTED

            Exit Function

        End If

        uSafeArray.lPointer = Memory_CopyByVal4(lSrcArrayPtr + 8&)

        If uSafeArray.lVarType And POINTER_TO_POINTER Then 'if pointer to pointer

            uSafeArray.lPointer = Memory_CopyByVal4(uSafeArray.lPointer)
            uSafeArray.lVarType = uSafeArray.lVarType Xor POINTER_TO_POINTER

        End If

    End If

    If uSafeArray.lPointer Then

        Memory_Copy uSafeArray, ByVal uSafeArray.lPointer, SAFEARRAY_OFFSET

        ReDim uSafeArray.uBounds(uSafeArray.iDims - 1&)

        Memory_Copy uSafeArray.uBounds(0), ByVal uSafeArray.lPointer + SAFEARRAY_OFFSET, uSafeArray.iDims * 8& 'get array dimensions info bytes (in descending order) starting from array pointer adress + offset

    Else

        ArrayGetInfo = E_POINTER

    End If

End Function
