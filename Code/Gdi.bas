Attribute VB_Name = "Z_Gdi"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' Dependencies:
'
'  WinAPI\Shared\Core\Enums\Ole.bas
'  WinAPI\Shared\Core\Types\Gdi.bas
'  WinAPI\Shared\Core\Gdi.bas
'  WinAPI\Shared\Gdi.bas
'  WinAPI\Shared\GdiPlus.bas
'  WinAPI\Shared\SafeArray.bas
'  WinAPI\Guid.bas
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'METHODS

Public Function Gdi_LoadPictureFromBytes(ByRef lBytes As Variant, ByRef oPicture As Object) As Boolean

    On Error GoTo ErrorHandler

    Dim l_Bitmap As Long
    Dim l_BitmapPtr As Long
    Dim l_Dc As Long
    Dim l_Image As Long
    Dim l_Graphics As Long
    Dim l_Picture As Object
    Dim l_Rect As TRECT
    Dim l_SafeArr As TSAFEARRAY
    Dim l_Token As Long

    SafeArray_FillInfo l_SafeArr, VarPtr(lBytes)

    If l_SafeArr.iDims = 1 And l_SafeArr.lVarType = vbByte Then

        If Gdi_CreateStream(l_SafeArr.lData, True, l_Picture) = S_OK Then

            If GdiPlus_Start(l_Token) = Ok Then

                If GdiPlus_CreateImageFromStream(ObjPtr(l_Picture), l_Image) = Ok Then

                    GdiPlus_GetImageSize l_Image, l_Rect.lBottom, l_Rect.lRight

                    l_Dc = Gdi_CreateDC
                    l_Bitmap = Gdi_CreateBitmap(l_Rect.lRight, l_Rect.lBottom, Gdi_GetDeviceCaps(l_Dc, PLANES), Gdi_GetDeviceCaps(l_Dc, BITSPIXEL))
                    l_BitmapPtr = Gdi_SelectObject(l_Dc, l_Bitmap)

                    GdiPlus_CreateGraphics l_Dc, l_Graphics
                    GdiPlus_DrawImage l_Graphics, l_Image, 0&, 0&, l_Rect.lRight, l_Rect.lBottom

                    Gdi_SelectObject l_Dc, l_BitmapPtr

                    Gdi_DeleteObject l_BitmapPtr
                    Gdi_DeleteDC l_Dc
                    GdiPlus_DeleteImage l_Image
                    GdiPlus_DeleteGraphics l_Graphics

                    If Gdi_CreatePictureIndirect(Guid_GetIPicture, l_Bitmap, PICTYPE_BITMAP, 0&, True, l_Picture) = S_OK Then Set oPicture = l_Picture

                End If

                GdiPlus_Terminate l_Token

            Else

                If Gdi_CreatePictureFromStream(ObjPtr(l_Picture), SafeArray_GetElementsCount(l_SafeArr, 1&), 0&, Guid_GetIPicture, l_Picture) = S_OK Then
                
                    Set oPicture = l_Picture
                    
                    Gdi_LoadPictureFromBytes = True
                    
                End If

            End If

        End If

    End If

ErrorHandler:

    Set l_Picture = Nothing

End Function
